import pygame as pg
import random
import os


import math

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
    quit()
# Redefine math.f() to f()
def sqrt(x):
    return math.sqrt(x)
def sin(x):
    return math.sin(x)
def cos(x):
    return math.cos(x)
# Redefine pi to something shorter
pi = math.pi
print ''







# n squares wide, high; and width, height of square in pixels
gridwidth = 80
gridheight = 100
squarewidth = squareheight = 5 

# border size between squares in pixels
nmargin = gridwidth+1
squaremargin = 0

# Initialize empty zeros grid
grid = [[0]*gridwidth for i in range(gridheight)]








### Following Voronoi/Single/VoronoiMake2.py





### Plan: Attempt 2
# 1. Use voronoi (of some form) to create territories on map
#   maybe use 0,1,2,3... Ntotalterritories for id. If id==0, make it "water". 
# 2. Assign territories to teams
# 3. make borders on territories
# 4. When click on tarritory, it knows of whole territory.
# 5. give random dice to each territory...

'''
pygame.draw.polygon()
draw a shape with any number of sides
polygon(Surface, color, pointlist, width=0) -> Rect
'''

'''
pygame.draw.lines()
draw multiple contiguous line segments
lines(Surface, color, closed, pointlist, width=1) -> Rect
Draw a sequence of lines on a Surface. The pointlist argument is a series of points that are connected by a line. If the closed argument is true an additional line segment is drawn between the first and last points.
'''

# N number of teams/colors/identifiers.
NTeams = 5
# TeamIdentifiers. Team 0, Team 1... 
TeamIdentifiers = xrange(NTeams)


# N number of territories each team/color/identifier begins with.
NTerritoriesPerTeam = 4
# N number of empty void "territories".
NVoids = 3*NTeams
# N numbr of total territories on the map.
NTerritories = NTeams*NTerritoriesPerTeam+NVoids
# TerritoryIdentifiers. Territory 0, 1... "NTerritories+1" is the empty space territory identifier
TerritoryIdentifiers = xrange(NTerritories)
# Seed locations for Territories. Territories = [(14, 13), (13, 13), ... ]
TerritorySeeds = [(random.randint(0,gridwidth-1), random.randint(0,gridheight-1)) for i in TerritoryIdentifiers]


# Function to salt (modify) grid with seeds
def Salt(seeds, ids):
    # seeds = [(y0,x0), (y1,x1), ...]
    # ids = [0,1, ...]
    for (x,y), i in zip(seeds,ids):
        grid[y][x] = i
        

# Now do Voronoi
def Voronoi(seeds,ids):
    # seeds = [(y0,x0), (y1,x1), ...]
    # ids = [0,1, ...]
    Territories = []
    for y0 in range(gridheight):
        for x0 in range(gridwidth):
            rArray = []
            for (x,y),i in zip(seeds,ids):
                r = sqrt( abs(y-y0) + abs(x-x0) ) # Manhattan
                rArray.append(r)
            grid[y0][x0] = ids[rArray.index(min(rArray))]


# Initialize empty zeros grid
grid = [[0]*gridwidth for i in range(gridheight)]

# Recreate territory map
Salt(TerritorySeeds,TerritoryIdentifiers)
Voronoi(TerritorySeeds,TerritoryIdentifiers)


# Now assign put borders on created territories... 
'''
def GetTerritories(g,ids):
    # g = [[id1,id2,e.g...],[...]] == grid
    # ids = [0,1, ...]
    Territories = []
    for y0 in xrange(gridheight):
        for x0 in xrange(gridheight):
            Territories.append( ((y0,x0), g[y0][x0]) ) 
    return Territories

# Map[0] = ((0,0), 22), e.g. == ((y,x),id)
Map = GetTerritories(grid, TerritoryIdentifiers)
'''



### TerritoryIdentifiers and corresponding positions. Flat lists
'''
TerritoryIdentifiersFlat = [item for sublist in grid for item in sublist]
TerritoryIdentifiersPos = 
'''


# I think I want something like this:
# Map = [ [id0, (y0,x0), (y1,x1), ...)], [id1...] ]
def GetTerrGridPositions(g,id):
    # g = [[id1,id2,e.g...],[...]] == grid
    # ids = [0,1, ...]
    Territories = [[id] for id in TerritoryIdentifiers]
    for y in xrange(gridheight):
        for x in xrange(gridwidth):
            Territories[g[y][x]].append( [y,x] )
    return Territories

#Map = GetTerrGridPositions(grid, TerritoryIdentifiers)


def Grid2Pixel(d,dim):
    if dim == 'x': return (squarewidth + squaremargin)*d #+ squaremargin
    else: return (squareheight + squaremargin)*d

#def Pixel2Grid(d):
#    return p/(squarewidth+squaremargin) #(p-squaremargin)/(squarewidth+squaremargin)



# I think I want something like this:
# Map = [ [id0, (y0,x0), (y1,x1), ...)], [id1...] ]
def GetTerrScreenPositions(g,id):
    # g = [[id1,id2,e.g...],[...]] == grid
    # ids = [0,1, ...]
    Territories = [[id] for id in TerritoryIdentifiers]
    for y in xrange(gridheight):
        for x in xrange(gridwidth):
            # For whatever reason, it wants x,y here to match up with grid.
            Territories[g[y][x]].append( [Grid2Pixel(x,dim='x'),Grid2Pixel(y,dim='y')] )
    return Territories

Map = GetTerrScreenPositions(grid, TerritoryIdentifiers)



### Territories and Voids
Territories = Map[:]
# Pick random group from Map to be Voids
#Voids = random.sample(Map,2)
Voids = [Territories.pop(random.randint(0,len(Territories)-1)) for i in range(NVoids)]





# Assume pos is top left corner of square, 0
def GetSquares(list):
    # list = [id, [x0,y0], [x1,y1] ...]
    newlist = [list[0]]
    for x0,y0 in list[1:]:
        # 0__1
        # |  |
        # 3__2
        x1,y1 = x0+squarewidth+squaremargin, y0
        x2,y2 = x0+squarewidth+squaremargin, y0+squareheight+squaremargin
        x3,y3 = x0, y0+squareheight+squaremargin
        newlist.append(  [[x0,y0],[x1,y1],[x2,y2],[x3,y3]] )
    return newlist


### Need to sort Terr, Voids lest the polygons be jagged... 
# By (min y, min x), then by (min y, max x), then (max y, max x), then (max y, min x). Clockwise loop.

### Maybe choose which xy's to use by finding center of mass, and choosing farthest points from it?


def GetCenter(list):
    # list = [id, [x0,y0], [x1,y1] ...]
    y = [i[0] for i in list[1:]]
    x = [i[1] for i in list[1:]]
    cy = sum(y)/float(len(y))
    cx = sum(x)/float(len(x))
    return cx,cy




# Return three farthest corners from center pos
'''
def Get3Farthest(cx,cy, corners):
    # corners = [id, [[x0,y0], [x1,y1], [x2,y2], [x3,y3]] ] of one square
    darray = []
    for x,y in 
        d = abs(x-cx)+abs(y-cy)
        darray.append(d)
    return darray[-3:]
'''





def euclideanDistance(instance1, instance2, length):
        distance = 0
        for x in range(length):
                distance += pow((instance1[x] - instance2[x]), 2)
        return math.sqrt(distance)

def getNeighbors(trainingSet, testInstance):
        distances = []
        length = len(testInstance)-1
        for x in range(len(trainingSet)):
                dist = euclideanDistance(testInstance, trainingSet[x], length)
                distances.append((trainingSet[x], dist))
        distances.sort(key=operator.itemgetter(1), reverse=True)
        neighbors = []
        for x in range(3):
                neighbors.append(distances[x][0])
        return neighbors

import operator
trainingSet = [[185, 440], [190, 440], [190, 445], [185, 445]]
testInstance = [468.41059602649005, 190.66225165562915]
getNeighbors(trainingSet, testInstance)








Voids2 = []
for v in Voids[:2]:
    cx,cy = GetCenter(v)
    vsquares = GetSquares(v)
    id = vsquares[0]
    squares = vsquares[1:]
    farthest = []
    for square in squares:
        farthest.append( getNeighbors(square, [cx,cy]) )
    Voids2.append( [id]+farthest )



#[item for sublist in Voids2[0][1:] for item in sublist]



Voids3 = [item for sublist in Voids2[-1][1:] for item in sublist]








'''
### From https://stackoverflow.com/questions/41855695/sorting-list-of-two-dimensional-coordinates-by-clockwise-angle-using-python
def clockwiseangle_and_distance(point):
    # Vector between point and the origin: v = p - o
    vector = [point[0]-origin[0], point[1]-origin[1]]
    # Length of vector: ||v||
    lenvector = math.hypot(vector[0], vector[1])
    # If length is zero there is no angle
    if lenvector == 0:
        return -math.pi, 0
    # Normalize vector: v/||v||
    normalized = [vector[0]/lenvector, vector[1]/lenvector]
    dotprod  = normalized[0]*refvec[0] + normalized[1]*refvec[1]     # x1*x2 + y1*y2
    diffprod = refvec[1]*normalized[0] - refvec[0]*normalized[1]     # x1*y2 - y1*x2
    angle = math.atan2(diffprod, dotprod)
    # Negative angles represent counter-clockwise angles so we need to subtract them 
    # from 2*pi (360 degrees)
    if angle < 0:
        return 2*math.pi+angle, lenvector
    # I return first the angle because that's the primary sorting criterium
    # but if two vectors have the same angle then the shorter distance should come first.
    return angle, lenvector



origin = [cx,cy]
refvec = [0, 1]
pts = Voids3
Voids4 = sorted(pts, key=clockwiseangle_and_distance)

refvec = [1,1]
Voids5 = sorted(Voids[0][1:], key=clockwiseangle_and_distance)
'''



'''
    Array = []
    for square in vsquares[1:]:
        [vsquares[0]]+[square[k] for k in [j[1] for j in sorted([(abs(x-cx)+abs(y-cy),i) for i,(x,y) in enumerate(vsquares[1:][0])])[1:]]]


        [abs(x-cx)+abs(y-cy) for i,(x,y) in enumerate(square)]

        array = []
        for x,y in square:
            array.append( (square, abs(y-cy) + abs(x-cx)))
        Array.append(sorted(array)[1:])

        rArray = []
            for (x,y),i in zip(seeds,ids):
                r = sqrt( abs(y-y0) + abs(x-x0) ) # Manhattan
                rArray.append(r)
'''






### Available colors of teams
#cnames = ['darkviolet', 'red', 'orange', 'yellow']
cnames = ['blue', 'green', 'red', 'cyan', 'magenta', 'yellow']



# Screen width, height in pixels
screenwidth = gridwidth*squarewidth + nmargin*squaremargin
screenheight = gridheight*squareheight + nmargin*squaremargin
screen = pg.display.set_mode([screenwidth, screenheight])

#pg.display.flip()
#pg.display.set_caption(os.path.basename(__file__)[:-3])

# Set screen background
screen.fill(pg.Color('dimgray'))

running = True
try:
    while running:

        # Draw grid
        #'''
        for row in range(gridheight):
            for column in range(gridwidth):
                color = pg.Color('white')
                for i in TerritoryIdentifiers:
                    f = 255./TerritoryIdentifiers[-1]
                    if grid[row][column] == i: color = (f*i,0,f*i)# (r,g,b) k=0,w=225 #pg.Color(cnames[i-1])
                #if grid[row][column] == 1: color = pg.Color('orange')
                #if grid[row][column] == 2: color = pg.Color('red')
                #if grid[row][column] == 3: color = pg.Color('darkviolet')
                pg.draw.rect(screen, color, [(squarewidth + squaremargin)*column + squaremargin, 
                                             (squareheight + squaremargin)*row + squaremargin, 
                                              squarewidth, squareheight]) #,1)
        # Draw polygon(s)
        pg.draw.polygon(screen, pg.Color('white'), Voids3 )
        #for i in Territories:
        #    pg.draw.polygon(screen, pg.Color('orange'), i[1:])
        #for void in Voids:
        #    pg.draw.polygon(screen, pg.Color('white'), void[1:])
        

        for event in pg.event.get():
            if event.type == pg.QUIT:
                running = False
            elif event.type == pg.MOUSEBUTTONDOWN:
                # pos from top left
                x,y = pg.mouse.get_pos()
                column = x/(squarewidth+squaremargin)
                row = y/(squareheight+squaremargin)
                grid[row][column] = 1 - grid[row][column]
                print 'Pixel coordiantes:', y,x, '\t Grid coordinates:', row, column
                

        pg.display.update()#flip()

    pg.quit()
except SystemExit:
    pg.quit()















