import pygame as pg
import random
import os


import math

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
    quit()
# Redefine math.f() to f()
def sqrt(x):
    return math.sqrt(x)
def sin(x):
    return math.sin(x)
def cos(x):
    return math.cos(x)
# Redefine pi to something shorter
pi = math.pi
print ''







# n squares wide, high; and width, height of square in pixels
gridwidth = gridheight = 50
squarewidth = squareheight = 10 

# border size between squares in pixels
nmargin = gridwidth+1
squaremargin = 0

# Screen width, height in pixels
(scwidth, scheight) = (gridwidth*squarewidth+nmargin*squaremargin, gridheight*squareheight+nmargin*squaremargin)
screen = pg.display.set_mode((scwidth, scheight))

# Initialize empty zeros grid
grid = [[0]*gridwidth for i in range(gridheight)]








### Following Voronoi/Single/VoronoiMake2.py
'''
# N number of teams/colors/identifiers
NTeams = 3

# Let salted values be identified by integers (instead of colors, eg). N random Salted values
NSalted = 100

def GenerateIndentifiers(nsalted):
    #return range(1,n+1)
    ids = [int(i) for i in np.random.uniform(1, NTeams+1, nsalted)]
    return ids

Identifiers = GenerateIndentifiers(NSalted)

### Define function so salt N points into matrix
def SaltFunction(dim,n):
    sf = [int(i) for i in np.random.uniform(0,dim,n)]
    return sf

### Generate random x,y position. Does not check of two points overlap.
xpos,ypos = SaltFunction(gridwidth, N), SaltFunction(gridheight, N)

### Change the above n random positions into n Identifiers
for xp,yp,i in zip(xpos,ypos,Identifiers):
  grid[yp][xp] = i

#[[random.randint(1,NTeams) for x in xrange(gridwidth)] for y in xrange(gridheight)]
'''
'''
# N number of teams/colors/identifiers. N number of territories each team/color/identifier begins with
NTeams = 6
NTerritories = 4

### Teams/Identifiers and beginning Territory locations. Territories[0] = [(14, 13), (13, 13), (12, 5)], e.g.
Identifiers = xrange(1,NTeams+1)
Territories = [[(random.randint(1,gridwidth), random.randint(1,gridheight)) for i in xrange(NTerritories)] for j in xrange(NTeams)]

### Modify grid to add the above.
for t,i in zip(Territories,Identifiers):
    for x,y in t:
        # random.randint(1,15) == 1,2,...15. But grid is only 0-14.
        grid[x-1][y-1] = i

### Now do Voronoi
for y0 in range(gridheight):
  for x0 in range(gridwidth):
    # [0,0], [0,1], ...
    # [1,0], [1,1], ...
    rArray = []
    for t in Territories:
        for x,y in t:
            r = sqrt( abs(x-x0) + abs(y-y0) ) # Manhattan
            rArray.append(r)
    grid[y0][x0] = Identifiers[rArray.index(min(rArray))/NTeams] # Works for any Identifiers list
'''


#Map = grid[:]










### Plan: Attempt 2
# 1. Use voronoi (of some form) to create territories on map
#   maybe use 0,1,2,3... Ntotalterritories for id. If id==0, make it "water". 
# 2. Assign territories to teams
# 3. make borders on territories
# 4. When click on tarritory, it knows of whole territory.
# 5. give random dice to each territory...

'''
pygame.draw.polygon()
draw a shape with any number of sides
polygon(Surface, color, pointlist, width=0) -> Rect
'''

'''
pygame.draw.lines()
draw multiple contiguous line segments
lines(Surface, color, closed, pointlist, width=1) -> Rect
Draw a sequence of lines on a Surface. The pointlist argument is a series of points that are connected by a line. If the closed argument is true an additional line segment is drawn between the first and last points.
'''

# N number of teams/colors/identifiers.
NTeams = 6
# N number of territories each team/color/identifier begins with.
NTerritoriesPerTeam = 5
# N numbr of total territories on the map. +1 because NTeams territories will be empty space.
NTerritories = NTeams*(NTerritoriesPerTeam+1)


# TeamIdentifiers. Team 1, Team 2... 
TeamIdentifiers = xrange(1,NTeams+1)
# TerritoryIdentifiers. Territory 0, 1... "NTerritories+1" is the empty space territory identifier
TerritoryIdentifiers = xrange(1,NTerritories+1+1)

# Seed locations for Territories. Territories = [(14, 13), (13, 13), ... ]
TerritorySeeds = [(random.randint(1,gridwidth), random.randint(1,gridheight)) for i in TerritoryIdentifiers]
#xrange(NTerritories+1)]


# Function to salt (modify) grid with seeds
def SetSalt(seeds, ids):
    # seeds = [(x0,y0), (x1,y1), ...]
    # ids = [1,2, ...]
    for (x,y), i in zip(seeds,ids):
        # random.randint(1,15) == 1,2,...15. But grid is only 0-14, e.g. I want Team 1,2... for whatever reason.
        grid[x-1][y-1] = i
        

# Now do Voronoi
def Voronoi(seeds,ids):
    for y0 in range(gridheight):
        for x0 in range(gridwidth):
            rArray = []
            for (x,y),i in zip(seeds,ids):
                r = sqrt( abs(x-x0) + abs(y-y0) ) # Manhattan
                rArray.append(r)
            grid[y0][x0] = ids[rArray.index(min(rArray))]


# Initialize empty zeros grid
grid = [[0]*gridwidth for i in range(gridheight)]

SetSalt(TerritorySeeds,TerritoryIdentifiers)
Voronoi(TerritorySeeds,TerritoryIdentifiers)


# Now assign put borders on created territories... in pixeldimension
def Grid2Pixel(d):
    return (squarewidth + squaremargin)*d + squaremargin
def Pixel2Grid(d):
    return p/(squarewidth+squaremargin) #(p-squaremargin)/(squarewidth+squaremargin)



### Available colors of teams
#cnames = ['darkviolet', 'red', 'orange', 'yellow']
cnames = ['blue', 'green', 'red', 'cyan', 'magenta', 'yellow']

#pg.display.flip()
#pg.display.set_caption(os.path.basename(__file__)[:-3])

# Set screen background
screen.fill(pg.Color('dimgray'))

running = True
try:
    while running:

        # Draw grid
        for row in range(gridheight):
            for column in range(gridwidth):
                color = pg.Color('white')
                for i in TerritoryIdentifiers:
                    f = 255./TerritoryIdentifiers[-1]
                    if grid[row][column] == i: color = (i*f,i*f,i*f)# (r,g,b) #pg.Color(cnames[i-1])
                #if grid[row][column] == 1: color = pg.Color('orange')
                #if grid[row][column] == 2: color = pg.Color('red')
                #if grid[row][column] == 3: color = pg.Color('darkviolet')
                pg.draw.rect(screen, color, [(squarewidth + squaremargin)*column + squaremargin, 
                                             (squareheight + squaremargin)*row + squaremargin, 
                                              squarewidth, squareheight]) #,1)

        for event in pg.event.get():
            if event.type == pg.QUIT:
                running = False
            elif event.type == pg.MOUSEBUTTONDOWN:
                # pos from top left
                x,y = pg.mouse.get_pos()
                column = x/(squarewidth+squaremargin)
                row = y/(squareheight+squaremargin)
                grid[row][column] = 1 - grid[row][column]
                print 'Pixel coordiantes:', y,x, '\t Grid coordinates:', row, column
                

        pg.display.update()#flip()

    pg.quit()
except SystemExit:
    pg.quit()















