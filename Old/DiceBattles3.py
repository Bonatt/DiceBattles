import pygame as pg
import random
import os


import math

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
    quit()
# Redefine math.f() to f()
def sqrt(x):
    return math.sqrt(x)
def sin(x):
    return math.sin(x)
def cos(x):
    return math.cos(x)
# Redefine pi to something shorter
pi = math.pi
print ''







# n squares wide, high; and width, height of square in pixels
gwidth = gheight = 15
sqwidth = sqheight = 20 

# border size between squares in pixels
nmargin = gwidth+1
sqmargin = 1

# Screen width, height in pixels
(scwidth, scheight) = (gwidth*sqwidth+nmargin*sqmargin, gheight*sqheight+nmargin*sqmargin)
screen = pg.display.set_mode((scwidth, scheight))

# Set screen background
screen.fill(pg.Color('dimgray'))


# Initialize empty zeros grid
#grid = [[0]*gwidth]*gheight # <-- stackoverflow.com/questions/24023115/how-to-initialise-a-2d-array-in-python
grid = [[0]*gwidth for i in range(gheight)]






### Following Voronoi/Single/VoronoiMake2.py
'''
# N number of teams/colors/identifiers
NTeams = 3

# Let salted values be identified by integers (instead of colors, eg). N random Salted values
NSalted = 100

def GenerateIndentifiers(nsalted):
    #return range(1,n+1)
    ids = [int(i) for i in np.random.uniform(1, NTeams+1, nsalted)]
    return ids

Identifiers = GenerateIndentifiers(NSalted)

### Define function so salt N points into matrix
def SaltFunction(dim,n):
    sf = [int(i) for i in np.random.uniform(0,dim,n)]
    return sf

### Generate random x,y position. Does not check of two points overlap.
xpos,ypos = SaltFunction(gwidth, N), SaltFunction(gheight, N)

### Change the above n random positions into n Identifiers
for xp,yp,i in zip(xpos,ypos,Identifiers):
  grid[yp][xp] = i

#[[random.randint(1,NTeams) for x in xrange(gwidth)] for y in xrange(gheight)]
'''
'''
# N number of teams/colors/identifiers. N number of territories each team/color/identifier begins with
NTeams = 3
NTerritories = 3

### Teams/Identifiers and beginning Territory locations. Territories[0] = [(14, 13), (13, 13), (12, 5)], e.g.
Identifiers = xrange(1,NTeams+1)
Territories = [[(random.randint(1,gwidth), random.randint(1,gheight)) for i in xrange(NTerritories)] for j in xrange(NTeams)]

### Modify grid to add the above.
for t,i in zip(Territories,Identifiers):
    for x,y in t:
        # random.randint(1,15) == 1,2,...15. But grid is only 0-14.
        grid[x-1][y-1] = i

### Now do Voronoi
for y0 in range(gheight):
  for x0 in range(gwidth):
    # [0,0], [0,1], ...
    # [1,0], [1,1], ...
    rArray = []
    for t in Territories:
        for x,y in t:
            r = sqrt( (x-x0)**2 + (y-y0)**2 )
            rArray.append(r)
    grid[y0][x0] = Identifiers[rArray.index(min(rArray))/NTeams] # Works for any Identifiers list
'''



#pg.display.flip()
#pg.display.set_caption(os.path.basename(__file__)[:-3])

running = True
try:
    while running:

        # Draw grid
        for row in range(gheight):
            for column in range(gwidth):
                color = pg.Color('white')
                if grid[row][column] == 1:
                    color = pg.Color('palegreen')
                pg.draw.rect(screen, color, [(sqwidth + sqmargin)*column + sqmargin, 
                                             (sqheight + sqmargin)*row + sqmargin, 
                                              sqwidth, sqheight])

        for event in pg.event.get():
            if event.type == pg.QUIT:
                running = False
            elif event.type == pg.MOUSEBUTTONDOWN:
                # pos from top left
                pos = pg.mouse.get_pos()
                column = pos[0]/(sqwidth+sqmargin)
                row = pos[1]/(sqheight+sqmargin)
                grid[row][column] = 1 - grid[row][column]
                print 'Pixel coordiantes:', pos, '\t Grid coordinates:', row, column
                

        pg.display.update()#flip()

    pg.quit()
except SystemExit:
    pg.quit()























### Return random roll of dice with number of sides. Has error handling.
def roll(dice,sides=3):
    if sides < 2 or dice < 1:
        raise ValueError('Roll at least one die with at least two sides.')
    return sum(random.randint(1, sides) for die in range(dice))



### Number of teams
NTeams = 2

### Team 1, 2, etc.
Teams = range(1,NTeams+1)

'''
### Battle! Team attacker and team defender
def battle(attacker, defender):
    # team = (id,dice)
    teams = attacker[0],defender[0)
    score = roll(attacker[1]), roll(defender[1])
    if score[0] == score[1]:
        print 'Team '+str(teams[1])+' won!'
    else:
        print 'Team '+str(teams[score.index(max(score))])+' won!'



    #for a,d in zip(atk,def):
    #    scores = (roll(a[1]),
        
'''


class Battle():
    def __init__(self, attacker, defender):
        # attacker = (team id, n dice)
        self.atk_id = attacker[0]
        self.atk_dice = attacker[1]
        self.def_id = defender[0]
        self.def_dice = defender[1]
    def winner(self):
        score = {}
        score[self.atk_id] = roll(self.atk_dice)
        score[self.def_id] = roll(self.def_dice)
        if score[self.atk_id] == score[self.def_id]:
            print 'Team '+str(self.def_id)+' won!'
        else:
            print 'Team '+str(max(score, key=score.get))+' won!' 


# For testing. Attacker aa is Team 1, two dice...
aa = (1,2)
dd = (2,2)
Battle(aa,dd).winner()
