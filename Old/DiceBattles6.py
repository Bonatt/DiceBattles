import pygame as pg
import random
import os


import math

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
    quit()
# Redefine math.f() to f()
def sqrt(x):
    return math.sqrt(x)
def sin(x):
    return math.sin(x)
def cos(x):
    return math.cos(x)
# Redefine pi to something shorter
pi = math.pi
print ''







# n squares wide, high; and width, height of square in pixels
gridwidth = gridheight = 50
squarewidth = squareheight = 10 

# border size between squares in pixels
nmargin = gridwidth+1
squaremargin = 0

# Initialize empty zeros grid
grid = [[0]*gridwidth for i in range(gridheight)]








### Following Voronoi/Single/VoronoiMake2.py
'''
# N number of teams/colors/identifiers
NTeams = 3

# Let salted values be identified by integers (instead of colors, eg). N random Salted values
NSalted = 100

def GenerateIndentifiers(nsalted):
    #return range(1,n+1)
    ids = [int(i) for i in np.random.uniform(1, NTeams+1, nsalted)]
    return ids

Identifiers = GenerateIndentifiers(NSalted)

### Define function so salt N points into matrix
def SaltFunction(dim,n):
    sf = [int(i) for i in np.random.uniform(0,dim,n)]
    return sf

### Generate random x,y position. Does not check of two points overlap.
xpos,ypos = SaltFunction(gridwidth, N), SaltFunction(gridheight, N)

### Change the above n random positions into n Identifiers
for xp,yp,i in zip(xpos,ypos,Identifiers):
  grid[yp][xp] = i

#[[random.randint(1,NTeams) for x in xrange(gridwidth)] for y in xrange(gridheight)]
'''
'''
# N number of teams/colors/identifiers. N number of territories each team/color/identifier begins with
NTeams = 6
NTerritories = 4

### Teams/Identifiers and beginning Territory locations. Territories[0] = [(14, 13), (13, 13), (12, 5)], e.g.
Identifiers = xrange(1,NTeams+1)
Territories = [[(random.randint(1,gridwidth), random.randint(1,gridheight)) for i in xrange(NTerritories)] for j in xrange(NTeams)]

### Modify grid to add the above.
for t,i in zip(Territories,Identifiers):
    for x,y in t:
        # random.randint(1,15) == 1,2,...15. But grid is only 0-14.
        grid[x-1][y-1] = i

### Now do Voronoi
for y0 in range(gridheight):
  for x0 in range(gridwidth):
    # [0,0], [0,1], ...
    # [1,0], [1,1], ...
    rArray = []
    for t in Territories:
        for x,y in t:
            r = sqrt( abs(x-x0) + abs(y-y0) ) # Manhattan
            rArray.append(r)
    grid[y0][x0] = Identifiers[rArray.index(min(rArray))/NTeams] # Works for any Identifiers list
'''


#Map = grid[:]










### Plan: Attempt 2
# 1. Use voronoi (of some form) to create territories on map
#   maybe use 0,1,2,3... Ntotalterritories for id. If id==0, make it "water". 
# 2. Assign territories to teams
# 3. make borders on territories
# 4. When click on tarritory, it knows of whole territory.
# 5. give random dice to each territory...

'''
pygame.draw.polygon()
draw a shape with any number of sides
polygon(Surface, color, pointlist, width=0) -> Rect
'''

'''
pygame.draw.lines()
draw multiple contiguous line segments
lines(Surface, color, closed, pointlist, width=1) -> Rect
Draw a sequence of lines on a Surface. The pointlist argument is a series of points that are connected by a line. If the closed argument is true an additional line segment is drawn between the first and last points.
'''

# N number of teams/colors/identifiers.
NTeams = 5
# TeamIdentifiers. Team 0, Team 1... 
TeamIdentifiers = xrange(NTeams)


# N number of territories each team/color/identifier begins with.
NTerritoriesPerTeam = 5
# N number of empty void "territories".
NVoids = 3*NTeams
# N numbr of total territories on the map.
NTerritories = NTeams*NTerritoriesPerTeam+NVoids
# TerritoryIdentifiers. Territory 0, 1... "NTerritories+1" is the empty space territory identifier
TerritoryIdentifiers = xrange(NTerritories)
# Seed locations for Territories. Territories = [(14, 13), (13, 13), ... ]
TerritorySeeds = [(random.randint(0,gridwidth-1), random.randint(0,gridheight-1)) for i in TerritoryIdentifiers]


# Function to salt (modify) grid with seeds
def Salt(seeds, ids):
    # seeds = [(y0,x0), (y1,x1), ...]
    # ids = [0,1, ...]
    for (y,x), i in zip(seeds,ids):
        grid[y][x] = i
        

# Now do Voronoi
def Voronoi(seeds,ids):
    # seeds = [(y0,x0), (y1,x1), ...]
    # ids = [0,1, ...]
    Territories = []
    for y0 in range(gridheight):
        for x0 in range(gridwidth):
            rArray = []
            for (y,x),i in zip(seeds,ids):
                r = sqrt( abs(y-y0) + abs(x-x0) ) # Manhattan
                rArray.append(r)
            grid[x0][y0] = ids[rArray.index(min(rArray))]
            #Territories.append( (grid[y0][x0], ids[rArray.index(min(rArray))]) ) 
    #return Territories

'''
def SetTerritories(gridpos,id):
    if 'Territories' not in vars(): Territories = []
    Territories.append((id,gridpos))
    #return Territories
'''

# Initialize empty zeros grid
grid = [[0]*gridwidth for i in range(gridheight)]

# Recreate territory map
Salt(TerritorySeeds,TerritoryIdentifiers)
Voronoi(TerritorySeeds,TerritoryIdentifiers)


# Now assign put borders on created territories... 
'''
def GetTerritories(g,ids):
    # g = [[id1,id2,e.g...],[...]] == grid
    # ids = [0,1, ...]
    Territories = []
    for y0 in xrange(gridheight):
        for x0 in xrange(gridheight):
            Territories.append( ((y0,x0), g[y0][x0]) ) 
    return Territories

# Map[0] = ((0,0), 22), e.g. == ((y,x),id)
Map = GetTerritories(grid, TerritoryIdentifiers)
'''



### TerritoryIdentifiers and corresponding positions. Flat lists
'''
TerritoryIdentifiersFlat = [item for sublist in grid for item in sublist]
TerritoryIdentifiersPos = 
'''


# I think I want something like this:
# Map = [ [id0, (y0,x0), (y1,x1), ...)], [id1...] ]
def GetTerrGridPositions(g,id):
    # g = [[id1,id2,e.g...],[...]] == grid
    # ids = [0,1, ...]
    Territories = [[id] for id in TerritoryIdentifiers]
    for y in xrange(gridheight):
        for x in xrange(gridwidth):
            Territories[g[y][x]].append( [y,x] )
    return Territories

#Map = GetTerrGridPositions(grid, TerritoryIdentifiers)


def Grid2Pixel(d):
    return (squarewidth + squaremargin)*d + squaremargin
def Pixel2Grid(d):
    return p/(squarewidth+squaremargin) #(p-squaremargin)/(squarewidth+squaremargin)



# I think I want something like this:
# Map = [ [id0, (y0,x0), (y1,x1), ...)], [id1...] ]
def GetTerrScreenPositions(g,id):
    # g = [[id1,id2,e.g...],[...]] == grid
    # ids = [0,1, ...]
    Territories = [[id] for id in TerritoryIdentifiers]
    for y in xrange(gridheight):
        for x in xrange(gridwidth):
            Territories[g[y][x]].append( [Grid2Pixel(y),Grid2Pixel(x)] )
    return Territories

Map = GetTerrScreenPositions(grid, TerritoryIdentifiers)



### Territories and Voids
Territories = Map[:]
# Pick random group from Map to be Voids
#Voids = random.sample(Map,2)
Voids = [Territories.pop(random.randint(0,len(Territories)-1)) for i in range(NVoids)]





### Need to sort Terr, Voids lest the polygons be jagged... 
# By (min y, min x), then by (min y, max x), then (max y, max x), then (max y, min x). Clockwise loop.



# From https://stackoverflow.com/questions/7673492/how-to-order-points-anti-clockwise
def order(a):
    from math import atan2
    arctangents=[]
    arctangentsandpoints=[]
    arctangentsoriginalsandpoints=[]
    arctangentoriginals=[]
    centerx=0
    centery=0
    sortedlist=[]
    firstpoint=[]
    k=len(a)
    for i in a:
        x,y=i[0],i[1]
        centerx+=float(x)/float(k)
        centery+=float(y)/float(k)
    for i in a:
        x,y=i[0],i[1]
        arctangentsandpoints+=[[i,atan2(y-centery,x-centerx)]]
        arctangents+=[atan2(y-centery,x-centerx)]
        arctangentsoriginalsandpoints+=[[i,atan2(y,x)]]
        arctangentoriginals+=[atan2(y,x)]
    arctangents=sorted(arctangents)
    arctangentoriginals=sorted(arctangentoriginals)
    for i in arctangents:
        for c in arctangentsandpoints:
            if i==c[1]:
                sortedlist+=[c[0]]
    for i in arctangentsoriginalsandpoints:
        if arctangentoriginals[0]==i[1]:
            firstpoint=i[0]
    z=sortedlist.index(firstpoint)
    m=sortedlist[:z]
    sortedlist=sortedlist[z:]
    sortedlist.extend(m)
    return sortedlist




v1 = Voids[0][1:]
v2 = order(v1)



# Sort points into convex hull:
'''
# From https://stackoverflow.com/questions/13935324/sorting-clockwise-polygon-points-in-matlab/13935419#13935419
#import numpy as np

y = [i[0] for i in v1]
x = [i[1] for i in v1]

# From https://stackoverflow.com/questions/9039961/finding-the-average-of-a-list
cy = sum(y)/float(len(y)) #np.mean(y)
cx = sum(x)/float(len(x)) #np.mean(x)

a = [math.atan2(yi-cy,xi-cx) for yi,xi in zip(y,x)]

sorted(a)...
y1,y = y,y1...
'''

from scipy.spatial import ConvexHull
'''
hull = ConvexHull(v1)
hulli = hull.vertices
v2 = [v1[i] for i in hulli]
'''

hullverts = [ConvexHull(v[1:]).vertices for v in Voids]
Voids2 = [[Voids[v][0]]+[Voids[v][i+1] for i in hullverts[v]] for v in range(len(Voids))]

hullverts = [ConvexHull(t[1:]).vertices for t in Territories]
Territories2 = [[Territories[t][0]]+[Territories[t][i+1] for i in hullverts[t]] for t in range(len(Territories))] 




### Available colors of teams
#cnames = ['darkviolet', 'red', 'orange', 'yellow']
cnames = ['blue', 'green', 'red', 'cyan', 'magenta', 'yellow']



# Screen width, height in pixels
screenwidth = gridwidth*squarewidth + nmargin*squaremargin
screenheight = gridheight*squareheight + nmargin*squaremargin

screen = pg.display.set_mode([screenwidth, screenheight])

#pg.display.flip()
#pg.display.set_caption(os.path.basename(__file__)[:-3])

# Set screen background
screen.fill(pg.Color('dimgray'))

running = True
try:
    while running:

        # Draw grid
        #'''
        for row in range(gridheight):
            for column in range(gridwidth):
                color = pg.Color('white')
                for i in TerritoryIdentifiers:
                    f = 255./TerritoryIdentifiers[-1]
                    if grid[row][column] == i: color = (f*i,0,f*i)# (r,g,b) k=0,w=225 #pg.Color(cnames[i-1])
                #if grid[row][column] == 1: color = pg.Color('orange')
                #if grid[row][column] == 2: color = pg.Color('red')
                #if grid[row][column] == 3: color = pg.Color('darkviolet')
                pg.draw.rect(screen, color, [(squarewidth + squaremargin)*column + squaremargin, 
                                             (squareheight + squaremargin)*row + squaremargin, 
                                              squarewidth, squareheight]) #,1)
        #'''
        # Draw polygon(s)
        #pg.draw.polygon(screen, pg.Color('white'), v2)#Voids[0][1:] )
        #'''
        for i in Territories:
            pg.draw.polygon(screen, pg.Color('orange'), i[1:])
        for i in Voids:
            pg.draw.polygon(screen, pg.Color('white'), i[1:])
        #'''

        for event in pg.event.get():
            if event.type == pg.QUIT:
                running = False
            elif event.type == pg.MOUSEBUTTONDOWN:
                # pos from top left
                x,y = pg.mouse.get_pos()
                column = x/(squarewidth+squaremargin)
                row = y/(squareheight+squaremargin)
                grid[row][column] = 1 - grid[row][column]
                print 'Pixel coordiantes:', y,x, '\t Grid coordinates:', row, column
                

        pg.display.update()#flip()

    pg.quit()
except SystemExit:
    pg.quit()















