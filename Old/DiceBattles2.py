import pygame as pg
import random
import os


import math

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
    quit()
# Redefine math.f() to f()
def sqrt(x):
    return math.sqrt(x)
def sin(x):
    return math.sin(x)
def cos(x):
    return math.cos(x)
# Redefine pi to something shorter
pi = math.pi
print ''







# n squares wide, high; and width, height of square in pixels
nwide = nhigh = 10
sqwidth = sqheight = 20 

# border size between squares in pixels
nmargin = nwide+1
sqmargin = 5

# Screen width, height in pixels
(scwidth, scheight) = (nwide*sqwidth+nmargin*sqmargin, nhigh*sqheight+nmargin*sqmargin)
screen = pg.display.set_mode((scwidth, scheight))


# Initialize empty zeros grid
#grid = [[0]*nwide]*nhigh # <-- stackoverflow.com/questions/24023115/how-to-initialise-a-2d-array-in-python
grid = [[0]*nwide for i in range(nhigh)]


#pg.display.flip()
#pg.display.set_caption(os.path.basename(__file__)[:-3])

running = True
try:
    while running:

        # Set screen background
        screen.fill(pg.Color('darkslategray'))

        # Draw grid
        for row in range(10):
            for column in range(10):
                color = pg.Color('white')
                if grid[row][column] == 1:
                    color = pg.Color('green')
                pg.draw.rect(screen, color,[(sqwidth + sqmargin)*column + sqmargin, 
                                            (sqheight + sqmargin)*row + sqmargin, sqwidth, sqheight])


        for event in pg.event.get():
            if event.type == pg.QUIT:
                running = False

            elif event.type == pg.MOUSEBUTTONDOWN:
                # pos from top left
                pos = pg.mouse.get_pos()
                column = pos[0]//(sqwidth+sqmargin)
                row = pos[1]//(sqheight+sqmargin)
                grid[row][column] = 1
                print 'Click', pos, 'Grid coordinates:', row, column

        pg.display.flip()

    pg.quit()
except SystemExit:
    pg.quit()























### Return random roll of dice with number of sides. Has error handling.
def roll(dice,sides=3):
    if sides < 2 or dice < 1:
        raise ValueError('Roll at least one die with at least two sides.')
    return sum(random.randint(1, sides) for die in range(dice))



### Number of teams
NTeams = 2

### Team 1, 2, etc.
Teams = range(1,NTeams+1)

'''
### Battle! Team attacker and team defender
def battle(attacker, defender):
    # team = (id,dice)
    teams = attacker[0],defender[0)
    score = roll(attacker[1]), roll(defender[1])
    if score[0] == score[1]:
        print 'Team '+str(teams[1])+' won!'
    else:
        print 'Team '+str(teams[score.index(max(score))])+' won!'



    #for a,d in zip(atk,def):
    #    scores = (roll(a[1]),
        
'''


class Battle():
    def __init__(self, attacker, defender):
        # attacker = (team id, n dice)
        self.atk_id = attacker[0]
        self.atk_dice = attacker[1]
        self.def_id = defender[0]
        self.def_dice = defender[1]
    def winner(self):
        score = {}
        score[self.atk_id] = roll(self.atk_dice)
        score[self.def_id] = roll(self.def_dice)
        if score[self.atk_id] == score[self.def_id]:
            print 'Team '+str(self.def_id)+' won!'
        else:
            print 'Team '+str(max(score, key=score.get))+' won!' 


# For testing. Attacker aa is Team 1, two dice...
aa = (1,2)
dd = (2,2)
Battle(aa,dd).winner()
