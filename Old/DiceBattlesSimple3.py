import pygame as pg
import random
import os


import math

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
    quit()
# Redefine math.f() to f()
def sqrt(x):
    return math.sqrt(x)
def sin(x):
    return math.sin(x)
def cos(x):
    return math.cos(x)
# Redefine pi to something shorter
pi = math.pi
print ''





#########################################################################################################
# Making simple Dice Battles game. No hex, no voronoi. Just squares... for now.
# See DiceBattles7 and 6 (and lower) for advances in those aspects.
#########################################################################################################






# n squares wide, high; and width, height of square in pixels
gridwidth = 5
gridheight = 5
squarewidth = squareheight = 75

# border size between squares in pixels
nmargin = gridwidth+1
squaremargin = 1











# N number of teams/colors/identifiers.
NTeams = 4


### N numbr of total Regions/Territories/Voids on the map. 
# Region == Territory (capturable) or Void (empty/obstruction)
NRegions = gridwidth*gridheight
# Percent/100 of map that territories cover
coverage = 0.8
NTerritories = int(round(NRegions*coverage))

### N number of Territories each team/color/identifier begins with.
NTerritoriesPerTeam = NTerritories/NTeams

NVoids = NRegions- NTerritoriesPerTeam*NTeams


### Generate dice for each Team/Territory
NStartingDicePerTeam = 3*NTerritoriesPerTeam










# From https://stackoverflow.com/questions/3589214/generate-multiple-random-numbers-to-equal-a-value-in-python
# Return a randomly chosen list of n positive integers summing to total.
# Each such list is equally likely to occur.
def CSS(n, total):
    dividers = sorted(random.sample(xrange(1, total), n - 1))
    return [a - b for a, b in zip(dividers + [total], [0] + dividers)]





def CreateMap(nteams, nterritoriesperteam, nvoids, nstartingdiceperteam):
    # Create starting locations list of form [1,1,1,1, 2,2,2,2,..., 0,0,...]
    # Team = 1, 2... Void = 0
    StartingLocations = sorted(range(1,nteams+1)*nterritoriesperteam) + [0]*nvoids
    # Create dice at starting locations of form [2,2,5,3, ..., 0,0,...]. 2+2+5+3 = 12 = NStartginDice here
    StartingLocationsDice = [CSS(nterritoriesperteam,nstartingdiceperteam) for i in xrange(nteams)]
    StartingLocationsDice = [item for sublist in StartingLocationsDice for item in sublist] + [0]*nvoids

    # Zip together and shuffle, then define as Map in shape of grid
    zipped = zip(StartingLocations,StartingLocationsDice)
    random.shuffle(zipped)

    map = [zipped[gridwidth*i:gridwidth*(i+1)] for i in xrange(gridheight)]
    return map






# Normalize Map so all non0 are 1, and all 0 are remain 0
def NormalizeMap(map):
    ID = 0
    mapnorm = [[int(round(j[ID]/(j[ID]+0.1))) for j in i] for i in map]
    return mapnorm






# Psuedohardcoding how to get neighbors. For reading reference: getting clockwise from top.
def GetNeighbors(matrix,row,column):
    if row == 0:
        if column == 0: neighbors = matrix[row][column+1], matrix[row+1][column]
        elif column == gridwidth-1: neighbors = matrix[row+1][column], matrix[row][column-1]
        else: neighbors = matrix[row][column+1], matrix[row+1][column], matrix[row][column-1]
    elif column == 0:
        if row == gridheight-1: neighbors = matrix[row-1][column], matrix[row][column+1]
        else: neighbors = matrix[row-1][column], matrix[row][column+1], matrix[row+1][column]
    elif row == gridheight-1:
        if column == gridwidth-1: neighbors = matrix[row-1][column], matrix[row][column-1]
        else: neighbors = matrix[row-1][column], matrix[row][column+1], matrix[row][column-1]
    elif column == gridwidth-1:
        neighbors = matrix[row-1][column], matrix[row+1][column], matrix[row][column-1]
    else:
        neighbors = matrix[row-1][column], matrix[row][column+1], matrix[row+1][column], matrix[row][column-1]
    return neighbors






### Check map for islands / nonpathable areas. This reall doesn't work. 
def CheckMap(map):
    # Minimum number,criteria of adjacent territories. If 2, will run forever...
    crit = 1
    MapNorm = NormalizeMap(map)
    passed = True
    for row in xrange(gridheight):
        for column in xrange(gridwidth):
            neighbors = GetNeighbors(MapNorm,row,column)
            if sum(neighbors) < crit:
                print 'Islands!'
                passed = False
                break
        if passed == False:
            break
    if passed == True:
        print 'No islands!'
    return passed




passed = False
while passed == False:
    Map = CreateMap(NTeams, NTerritoriesPerTeam, NVoids, NStartingDicePerTeam)
    passed = CheckMap(Map)








# Maybe for easier reading, assign indices 0,1 of Map with words instead
ID = 0
DICE = 1

























pg.font.init() # you have to call this at the start if you want to use this module.
fontsize = squarewidth/3
font = pg.font.SysFont('monospace', fontsize)

#text = font.render('Some Text', False, (0, 0, 0))























### Available colors of teams
#cnames = ['white','darkviolet', 'darkgreen', 'darkblue', 'yellow']
#cnames = ['white', 'palegreen', 'steelblue', 'firebrick', 'orchid']
cnames = ['white', 'navy', 'magenta', 'cyan','gold']
#cnames = ['white','blue', 'green', 'red', 'cyan', 'magenta', 'yellow']
cnames = ['white', 'blue','green','red','yellow']


# Screen width, height in pixels
screenwidth = gridwidth*squarewidth + nmargin*squaremargin
screenheight = gridheight*squareheight + nmargin*squaremargin
screen = pg.display.set_mode([screenwidth, screenheight])

#pg.display.flip()
#pg.display.set_caption(os.path.basename(__file__)[:-3])

# Set screen background
screen.fill(pg.Color('black'))

running = True
try:
    while running:

        # Draw grid
        for row in xrange(gridheight):
            for column in xrange(gridwidth):
                color = pg.Color(cnames[Map[row][column][ID]])
                #if grid[row][column] == 0: pg.Color('white')
                #elif grid[row][column] == 0:
                #color = pg.Color('white')
                #if grid[row][column] == 1: color = pg.Color('palegreen')
                #if grid[row][column] == 1: color = pg.Color('orange')
                #if grid[row][column] == 2: color = pg.Color('red')
                #if grid[row][column] == 3: color = pg.Color('darkviolet')
                pg.draw.rect(screen, color, [(squarewidth + squaremargin)*column + squaremargin, 
                                             (squareheight + squaremargin)*row + squaremargin, 
                                              squarewidth, squareheight]) #,1)
                if Map[row][column][ID] != 0:
                    #if len(StartingDice[grid[row][column]-1]) > 0 :
                    ndice = Map[row][column][DICE] #.pop()
                    #ndice = random.randint(1,5) 
                    dice = font.render(str(ndice), False, (0, 0, 0))
                    screen.blit(dice, ((squarewidth + squaremargin)*column + squaremargin + squarewidth/3,
                                        (squareheight + squaremargin)*row + squaremargin + squareheight/3))

        for event in pg.event.get():
            if event.type == pg.QUIT:
                running = False
            elif event.type == pg.MOUSEBUTTONDOWN:
                # pos from top left
                x,y = pg.mouse.get_pos()
                column = x/(squarewidth+squaremargin)
                row = y/(squareheight+squaremargin)
                pg.draw.rect(screen, pg.Color('black'), [(squarewidth + squaremargin)*column + squaremargin,
                                             (squareheight + squaremargin)*row + squaremargin,
                                              squarewidth, squareheight],10)

                #grid[row][column] = 1 - grid[row][column]
                print 'Screen/Pixels: y = '+str(y),'x = '+str(x),'\t Grid: row = '+str(row),'col = '+str(column)
                

        pg.display.update()#flip()

    pg.quit()
except SystemExit:
    pg.quit()















