import pygame as pg
import random
import os


import math

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
    quit()
# Redefine math.f() to f()
def sqrt(x):
    return math.sqrt(x)
def sin(x):
    return math.sin(x)
def cos(x):
    return math.cos(x)
# Redefine pi to something shorter
pi = math.pi
print ''





#########################################################################################################
# Making simple Dice Battles game. No hex, no voronoi. Just squares... for now.
# See DiceBattles7 and 6 (and lower) for advances in those aspects.
#########################################################################################################






# n squares wide, high; and width, height of square in pixels
gridwidth = 5
gridheight = 5
squarewidth = squareheight = 75

# border size between squares in pixels
nmargin = gridwidth+1
squaremargin = 1

# Initialize empty zeros grid
grid = [[0]*gridwidth for i in range(gridheight)]










### Plan: Attempt 2
# 1. Use voronoi (of some form) to create territories on map
#   maybe use 0,1,2,3... Ntotalterritories for id. If id==0, make it "water". 
# 2. Assign territories to teams
# 3. make borders on territories
# 4. When click on tarritory, it knows of whole territory.
# 5. give random dice to each territory...



# N number of teams/colors/identifiers.
NTeams = 4

# N number of Territories each team/color/identifier begins with.
NTerritoriesPerTeam = 4

# N numbr of total Regions/Territories/Voids on the map. 
# Region == Territory (capturable) or Void (empty/obstruction)
NRegions = gridwidth*gridheight
NTerritories = NTeams*NTerritoriesPerTeam
NVoids = NRegions- NTerritories



'''
# Team 0, Team 1... 
TeamIdentifiers = xrange(NTeams)
# Region 0, 1...
RegionIdentifiers = ['r'+str(i) for i in xrange(NRegions)] #xrange(NRegions)
# Territory 0, 1...
TerritoryIdentifiers = ['t'+str(i) for i in xrange(NTerritories)] #xrange(NTerritories)
# Void 0, 1...
VoidIdentifiers = ['v'+str(i) for i in xrange(NVoids)] #xrange(NVoids)

TVIdentifiers = TerritoryIdentifiers+VoidIdentifiers
random.shuffle(TVIdentifiers)

# Fill grid with identifiers at seed locations
for row in xrange(gridheight):
    for column in xrange(gridwidth):
        grid[row][column] = TVIdentifiers.pop()
'''

'''
# Seed locations for Regions. Regions = [(5, 3), (4, 0), (4, 1), ...]. (x=column,y=row)
RegionSeeds = [(random.randint(0,gridwidth-1), random.randint(0,gridheight-1)) for i in RegionIdentifiers]
# Since the Region seeds are random, just take from them to define Territory and Void seeds
TerritorySeeds = RegionSeeds[:NTerritories]
VoidSeeds = RegionSeeds[NTerritories:]
'''




# Create Identifiers list of form [1,2,3,..., 1,2,3,..., 0,0,0,...]
# Team = 1, 2... Void = 0
Identifiers = range(1,NTeams+1)*NTeams + [0]*NVoids

# Fill grid with identifiers randomly
random.shuffle(Identifiers)
for row in xrange(gridheight):
    for column in xrange(gridwidth):
        grid[row][column] = Identifiers.pop()




# From https://stackoverflow.com/questions/10305292/divide-the-number-into-random-number-of-random-elements
'''
def decomposition(i):
        while i > 0:
            n = random.randint(1, i)
            yield n
            i -= n
'''

### Generate dice for each Team/Territory
NStartingDicePerTeam = 3*NTerritoriesPerTeam

# From https://stackoverflow.com/questions/3589214/generate-multiple-random-numbers-to-equal-a-value-in-python
# Return a randomly chosen list of n positive integers summing to total.
# Each such list is equally likely to occur.
def constrained_sum_sample(n, total):
    dividers = sorted(random.sample(xrange(1, total), n - 1))
    return [a - b for a, b in zip(dividers + [total], [0] + dividers)]



StartingDice = [constrained_sum_sample(NTerritoriesPerTeam,NStartingDicePerTeam) for i in xrange(NTeams)]






############ I need to create a dictionary holding TeamID, Position, Dice, ...










pg.font.init() # you have to call this at the start if you want to use this module.
fontsize = 30
font = pg.font.SysFont('monospace', fontsize)

#text = font.render('Some Text', False, (0, 0, 0))
























### Available colors of teams
#cnames = ['white','darkviolet', 'darkgreen', 'darkblue', 'yellow']
#cnames = ['white', 'palegreen', 'steelblue', 'firebrick', 'orchid']
cnames = ['white', 'navy', 'magenta', 'cyan','gold']
#cnames = ['white','blue', 'green', 'red', 'cyan', 'magenta', 'yellow']
cnames = ['white', 'blue','green','red','yellow']


# Screen width, height in pixels
screenwidth = gridwidth*squarewidth + nmargin*squaremargin
screenheight = gridheight*squareheight + nmargin*squaremargin
screen = pg.display.set_mode([screenwidth, screenheight])

#pg.display.flip()
#pg.display.set_caption(os.path.basename(__file__)[:-3])

# Set screen background
screen.fill(pg.Color('black'))

running = True
try:
    while running:

        # Draw grid
        for row in xrange(gridheight):
            for column in xrange(gridwidth):
                color = pg.Color(cnames[grid[row][column]])
                #if grid[row][column] == 0: pg.Color('white')
                #elif grid[row][column] == 0:
                #color = pg.Color('white')
                #if grid[row][column] == 1: color = pg.Color('palegreen')
                #if grid[row][column] == 1: color = pg.Color('orange')
                #if grid[row][column] == 2: color = pg.Color('red')
                #if grid[row][column] == 3: color = pg.Color('darkviolet')
                pg.draw.rect(screen, color, [(squarewidth + squaremargin)*column + squaremargin, 
                                             (squareheight + squaremargin)*row + squaremargin, 
                                              squarewidth, squareheight]) #,1)
                if grid[row][column] != 0:
                    ndice = StartingDice[grid[row][column]-1][0]
                    #ndice = random.randint(1,5) 
                    dice = font.render(str(ndice), False, (0, 0, 0))
                    screen.blit(dice, ((squarewidth + squaremargin)*column + squaremargin + squarewidth/3,
                                      (squareheight + squaremargin)*row + squaremargin + squareheight/3))

        for event in pg.event.get():
            if event.type == pg.QUIT:
                running = False
            elif event.type == pg.MOUSEBUTTONDOWN:
                # pos from top left
                x,y = pg.mouse.get_pos()
                column = x/(squarewidth+squaremargin)
                row = y/(squareheight+squaremargin)
                grid[row][column] = 1 - grid[row][column]
                print 'Screen/Pixels: y = '+str(y),'x = '+str(x),'\t Grid: row = '+str(row),'col = '+str(column)
                

        pg.display.update()#flip()

    pg.quit()
except SystemExit:
    pg.quit()















