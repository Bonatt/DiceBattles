import pygame as pg
import random
import os


import math

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
    quit()
# Redefine math.f() to f()
def sqrt(x):
    return math.sqrt(x)
def sin(x):
    return math.sin(x)
def cos(x):
    return math.cos(x)
# Redefine pi to something shorter
pi = math.pi
print ''





#########################################################################################################
# Making simple Dice Battles game. No hex, no voronoi. Just squares... for now.
# See DiceBattles7 and 6 (and lower) for advances in those aspects.
#########################################################################################################






# n squares wide, high; and width, height of square in pixels
gridwidth = 5
gridheight = 5
squarewidth = squareheight = 75

# border size between squares in pixels
nmargin = gridwidth+1
squaremargin = 1

# Initialize empty zeros grid
grid = [[0]*gridwidth for i in range(gridheight)]










### Plan: Attempt 2
# 1. Use voronoi (of some form) to create territories on map
#   maybe use 0,1,2,3... Ntotalterritories for id. If id==0, make it "water". 
# 2. Assign territories to teams
# 3. make borders on territories
# 4. When click on tarritory, it knows of whole territory.
# 5. give random dice to each territory...



# N number of teams/colors/identifiers.
NTeams = 4

# N number of Territories each team/color/identifier begins with.
NTerritoriesPerTeam = 4

# N numbr of total Regions/Territories/Voids on the map. 
# Region == Territory (capturable) or Void (empty/obstruction)
NRegions = gridwidth*gridheight
NTerritories = NTeams*NTerritoriesPerTeam
NVoids = NRegions- NTerritories



'''
# Team 0, Team 1... 
TeamIdentifiers = xrange(NTeams)
# Region 0, 1...
RegionIdentifiers = ['r'+str(i) for i in xrange(NRegions)] #xrange(NRegions)
# Territory 0, 1...
TerritoryIdentifiers = ['t'+str(i) for i in xrange(NTerritories)] #xrange(NTerritories)
# Void 0, 1...
VoidIdentifiers = ['v'+str(i) for i in xrange(NVoids)] #xrange(NVoids)

TVIdentifiers = TerritoryIdentifiers+VoidIdentifiers
random.shuffle(TVIdentifiers)

# Fill grid with identifiers at seed locations
for row in xrange(gridheight):
    for column in xrange(gridwidth):
        grid[row][column] = TVIdentifiers.pop()
'''

'''
# Seed locations for Regions. Regions = [(5, 3), (4, 0), (4, 1), ...]. (x=column,y=row)
RegionSeeds = [(random.randint(0,gridwidth-1), random.randint(0,gridheight-1)) for i in RegionIdentifiers]
# Since the Region seeds are random, just take from them to define Territory and Void seeds
TerritorySeeds = RegionSeeds[:NTerritories]
VoidSeeds = RegionSeeds[NTerritories:]
'''




# Create Identifiers list of form [1,2,3,..., 1,2,3,..., 0,0,0,...]
# Team = 1, 2... Void = 0
Identifiers = range(1,NTeams+1)*NTeams + [0]*NVoids

# Fill grid with identifiers randomly
random.shuffle(Identifiers)
for row in xrange(gridheight):
    for column in xrange(gridwidth):
        grid[row][column] = Identifiers.pop()




# From https://stackoverflow.com/questions/10305292/divide-the-number-into-random-number-of-random-elements
'''
def decomposition(i):
        while i > 0:
            n = random.randint(1, i)
            yield n
            i -= n
'''

### Generate dice for each Team/Territory
NStartingDicePerTeam = 3*NTerritoriesPerTeam

# From https://stackoverflow.com/questions/3589214/generate-multiple-random-numbers-to-equal-a-value-in-python
# Return a randomly chosen list of n positive integers summing to total.
# Each such list is equally likely to occur.
def constrained_sum_sample(n, total):
    dividers = sorted(random.sample(xrange(1, total), n - 1))
    return [a - b for a, b in zip(dividers + [total], [0] + dividers)]



StartingDice = [constrained_sum_sample(NTerritoriesPerTeam,NStartingDicePerTeam) for i in xrange(NTeams)]







############ I need to create a dictionary holding TeamID, Position, Dice, ... New approach:

# Team 0, Team 1... 
TeamIdentifiers = xrange(NTeams)
# Region 0, 1...
RegionIdentifiers = ['r'+str(i) for i in xrange(NRegions)] #xrange(NRegions)
# Territory 0, 1...
TerritoryIdentifiers = ['t'+str(i) for i in xrange(NTerritories)] #xrange(NTerritories)
# Void 0, 1...
VoidIdentifiers = ['v'+str(i) for i in xrange(NVoids)] #xrange(NVoids)

MAP = {}
MAP['Teams'] = TeamIdentifiers






# Create starting locations list of form [1,1,1,1, 2,2,2,2,..., 0,0,...]
# Team = 1, 2... Void = 0
StartingLocations = sorted(range(1,NTeams+1)*NTeams) + [0]*NVoids

# Create dice at starting locations of form [2,2,5,3, ..., 0,0,...]. 2+2+5+3 = 12 = NStartginDice here
StartingLocationsDice = constrained_sum_sample(NTerritoriesPerTeam,NStartingDicePerTeam)*NTeams + [0]*NVoids

# Zip together and shuffle, then define as Map in shape of grid
zipped = zip(StartingLocations,StartingLocationsDice)
random.shuffle(zipped)

Map = [zipped[gridwidth*i:gridwidth*(i+1)] for i in xrange(gridheight)]


'''
# Zip together and shuffle. These will be popped off onto the Map list
zipped = zip(StartingLocations,StartingLocationsDice)
random.shuffle(zipped)

# MAP[row][column] list housing [ (TeamID/Void, Dice) ]
Map = [[0]*gridwidth for i in range(gridheight)]

# Instead of looping through Map, I'm sure I could just reshape zipped. Whatever.
for row in xrange(gridheight):
    for column in xrange(gridwidth):
        Map[row][column] = zipped.pop()
'''

# Maybe for easier reading, assign indices 0,1 of Map with words instead
ID = 0
DICE = 1




'''
for i in Map: [j[ID] for j in i]
[[j[ID]*100 for j in i] for i in Map]
'''

# Normalize Map so all non0 are 1, and all 0 are remain 0
def NormalizeMap(map):
    mapnorm = [[int(round(j[ID]/(j[ID]+0.1))) for j in i] for i in map]
    return mapnorm

'''
for i in MapNorm: i
'''




# Maybe to prove that there exist no blocked off areas: 
# prove no other territory is more than 1 block away, manhattan dist.
# check Map[0][0]

'''
def GetNeighbors(matrix,row,column):
    # pos = (row=0,column=0) or greater
    home = (row,column)
    neighbors = []
    if row == 0 and column == 0:
        for neighbor in neighborhood[1:-1]:
            y,x = neighbor
            neighbors.append(matrix[y][x])
    elif column == 0:
        for neighbor in neighborhood[1:]:
            y,x = neighbor
            neighbors.append(matrix[y][x])
    elif row == 0:
        for neighbor in neighborhood[:-1]:
            y,x = neighbor
            neighbors.append(matrix[y][x])

    elif row == gridheight-1 and column == gridwidth-1:
        for neighbor in [neighborhood[0],neighborhood[-1]]:


    elif row == gridheight-1:
        for neighbor in [[neighborhood[0]]+neighborhood[2:]]:

    elif column == gridwidth-1:    
        for neighbor in [neighborhood[:2]+[neighborhood[-1]]]:

    else:
        for neighbor in neighborhood:
            y,x = neighbor
            neighbors.append(matrix[y][x])

    return neighbors
'''



# Psuedohardcoding how to get neighbors. For reading reference: getting clockwise from top.
def GetNeighbors(matrix,row,column):
    if row == 0:
        if column == 0: neighbors = matrix[row][column+1], matrix[row+1][column]
        elif column == gridwidth-1: neighbors = matrix[row+1][column], matrix[row][column-1]
        else: neighbors = matrix[row][column+1], matrix[row+1][column], matrix[row][column-1]
    elif column == 0:
        if row == gridheight-1: neighbors = matrix[row-1][column], matrix[row][column+1]
        else: neighbors = matrix[row-1][column], matrix[row][column+1], matrix[row+1][column]
    elif row == gridheight-1:
        if column == gridwidth-1: neighbors = matrix[row-1][column], matrix[row][column-1]
        else: neighbors = matrix[row-1][column], matrix[row][column+1], matrix[row][column-1]
    elif column == gridwidth-1:
        neighbors = matrix[row-1][column], matrix[row+1][column], matrix[row][column-1]
    else:
        neighbors = matrix[row-1][column], matrix[row][column+1], matrix[row+1][column], matrix[row][column-1]
    return neighbors

    '''

    if row == 0 and column == 0:
        neighbors = matrix[row][column+1], matrix[row+1][column]

    elif column == 0:
        if row = 
        else: neighbors = matrix[row-1][column], matrix[row][column+1], matrix[row+1][column]

    elif row == 0:
        neighbors = matrix[row][column+1], matrix[row-1][column], matrix[row][column-1]

    elif row == gridheight-1 and column == gridwidth-1:
        neighbors = matrix[row][column-1], matrix[row-1][column]

    elif row == gridheight-1:
        neighbors = matrix[row-1][column], matrix[row][column+1], matrix[row][column-1]

    elif column == gridwidth-1:    
        neighbors = matrix[row-1][column], matrix[row][column+1], matrix[row][column-1]

    else:
    '''

















# From HotCoffeeDrippingOnIce4
### Create matrix for neighbors. Top left is (y=-1,x=-1) and bottom right is (y=1,x=1).
#neighborhood = [ (-1,-1), (0,-1), (1,-1), (-1,0), (1,0), (-1,1), (0,1), (1,1) ] <-- Diag kosher
#neighborhood = [ (0,-1), (-1,0), (1,0), (0,1) ] # <-- Diag nonkosher
#neighborhood = [ (0,-1), (1,0), (0,1), (-1,0) ] # <-- better order for GetNeighbors
# Maybe neighborhood isn't a useful definition...




### Check map for islands / nonpathable areas
def CheckMap(map):
    MapNorm = NormalizeMap(map)
    for row in xrange(gridheight):
        for column in xrange(gridwidth):
            neighbors = GetNeighbors(MapNorm,row,column)
            if sum(neighbors) == 0:
                print 'Islands!'
                break








'''
row = 0
column = 0
home = (row,column)
matrix = MapNorm

neighbor = neighborhood[1:-1][0]
y,x = neighbor
'''


































pg.font.init() # you have to call this at the start if you want to use this module.
fontsize = squarewidth/3
font = pg.font.SysFont('monospace', fontsize)

#text = font.render('Some Text', False, (0, 0, 0))























### Available colors of teams
#cnames = ['white','darkviolet', 'darkgreen', 'darkblue', 'yellow']
#cnames = ['white', 'palegreen', 'steelblue', 'firebrick', 'orchid']
cnames = ['white', 'navy', 'magenta', 'cyan','gold']
#cnames = ['white','blue', 'green', 'red', 'cyan', 'magenta', 'yellow']
cnames = ['white', 'blue','green','red','yellow']


# Screen width, height in pixels
screenwidth = gridwidth*squarewidth + nmargin*squaremargin
screenheight = gridheight*squareheight + nmargin*squaremargin
screen = pg.display.set_mode([screenwidth, screenheight])

#pg.display.flip()
#pg.display.set_caption(os.path.basename(__file__)[:-3])

# Set screen background
screen.fill(pg.Color('black'))

running = True
try:
    while running:

        # Draw grid
        for row in xrange(gridheight):
            for column in xrange(gridwidth):
                color = pg.Color(cnames[Map[row][column][ID]])
                #if grid[row][column] == 0: pg.Color('white')
                #elif grid[row][column] == 0:
                #color = pg.Color('white')
                #if grid[row][column] == 1: color = pg.Color('palegreen')
                #if grid[row][column] == 1: color = pg.Color('orange')
                #if grid[row][column] == 2: color = pg.Color('red')
                #if grid[row][column] == 3: color = pg.Color('darkviolet')
                pg.draw.rect(screen, color, [(squarewidth + squaremargin)*column + squaremargin, 
                                             (squareheight + squaremargin)*row + squaremargin, 
                                              squarewidth, squareheight]) #,1)
                if Map[row][column][ID] != 0:
                    #if len(StartingDice[grid[row][column]-1]) > 0 :
                    ndice = Map[row][column][DICE] #.pop()
                    #ndice = random.randint(1,5) 
                    dice = font.render(str(ndice), False, (0, 0, 0))
                    screen.blit(dice, ((squarewidth + squaremargin)*column + squaremargin + squarewidth/3,
                                        (squareheight + squaremargin)*row + squaremargin + squareheight/3))

        for event in pg.event.get():
            if event.type == pg.QUIT:
                running = False
            elif event.type == pg.MOUSEBUTTONDOWN:
                # pos from top left
                x,y = pg.mouse.get_pos()
                column = x/(squarewidth+squaremargin)
                row = y/(squareheight+squaremargin)
                pg.draw.rect(screen, pg.Color('black'), [(squarewidth + squaremargin)*column + squaremargin,
                                             (squareheight + squaremargin)*row + squaremargin,
                                              squarewidth, squareheight],10)

                #grid[row][column] = 1 - grid[row][column]
                print 'Screen/Pixels: y = '+str(y),'x = '+str(x),'\t Grid: row = '+str(row),'col = '+str(column)
                

        pg.display.update()#flip()

    pg.quit()
except SystemExit:
    pg.quit()















