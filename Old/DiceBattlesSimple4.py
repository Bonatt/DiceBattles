import pygame as pg
import random
import os


import math

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
    quit()
# Redefine math.f() to f()
def sqrt(x):
    return math.sqrt(x)
def sin(x):
    return math.sin(x)
def cos(x):
    return math.cos(x)
# Redefine pi to something shorter
pi = math.pi
print ''





#########################################################################################################
# Making simple Dice Battles game. No hex, no voronoi. Just squares... for now.
# See DiceBattles7 and 6 (and lower) for advances in those aspects.
#########################################################################################################






# n squares wide, high; and width, height of square in pixels
gridwidth = 6
gridheight = 5
squarewidth = squareheight = 70

# border size between squares in pixels
nmargin = gridwidth+1
squaremargin = 1











# N number of teams/colors/identifiers.
NTeams = 3


### N numbr of total Regions/Territories/Voids on the map. 
# Region == Territory (capturable) or Void (empty/obstruction)
NRegions = gridwidth*gridheight
# Percent/100 of map that territories cover2
if NRegions < 25: coverage = 0.8
else: coverage = 0.6
NTerritories = int(round(NRegions*coverage))

### N number of Territories each team/color/identifier begins with.
NTerritoriesPerTeam = NTerritories/NTeams

NVoids = NRegions- NTerritoriesPerTeam*NTeams


### Generate dice for each Team/Territory
NStartingDicePerTeam = 3*NTerritoriesPerTeam










# From https://stackoverflow.com/questions/3589214/generate-multiple-random-numbers-to-equal-a-value-in-python
# Return a randomly chosen list of n positive integers summing to total.
# Each such list is equally likely to occur.
def CSS(n, total):
    dividers = sorted(random.sample(xrange(1, total), n - 1))
    return [a - b for a, b in zip(dividers + [total], [0] + dividers)]





def CreateMap(nteams, nterritoriesperteam, nvoids, nstartingdiceperteam):
    # Create starting locations list of form [1,1,1,1, 2,2,2,2,..., 0,0,...]
    # Team = 1, 2... Void = 0
    StartingLocations = sorted(range(1,nteams+1)*nterritoriesperteam) + [0]*nvoids
    # Create dice at starting locations of form [2,2,5,3, ..., 0,0,...]. 2+2+5+3 = 12 = NStartginDice here
    StartingLocationsDice = [CSS(nterritoriesperteam,nstartingdiceperteam) for i in xrange(nteams)]
    StartingLocationsDice = [item for sublist in StartingLocationsDice for item in sublist] + [0]*nvoids

    # Zip together, shuffle, recast tuple(a,b) to list[a,b], and define as Map in shape of grid
    zipped = zip(StartingLocations,StartingLocationsDice)
    zipped = [list(i) for i in zipped]
    random.shuffle(zipped)
    
    map = [zipped[gridwidth*i:gridwidth*(i+1)] for i in xrange(gridheight)]
    return map






# Normalize Map so all non0 are 1, and all 0 are remain 0
def NormalizeMap(map):
    ID = 0
    mapnorm = [[int(round(j[ID]/(j[ID]+0.1))) for j in i] for i in map]
    return mapnorm






# Psuedohardcoding how to get neighbors. For reading reference: getting clockwise from top.
def GetNeighbors(matrix,row,column):
    if row == 0:
        if column == 0: neighbors = matrix[row][column+1], matrix[row+1][column]
        elif column == gridwidth-1: neighbors = matrix[row+1][column], matrix[row][column-1]
        else: neighbors = matrix[row][column+1], matrix[row+1][column], matrix[row][column-1]
    elif column == 0:
        if row == gridheight-1: neighbors = matrix[row-1][column], matrix[row][column+1]
        else: neighbors = matrix[row-1][column], matrix[row][column+1], matrix[row+1][column]
    elif row == gridheight-1:
        if column == gridwidth-1: neighbors = matrix[row-1][column], matrix[row][column-1]
        else: neighbors = matrix[row-1][column], matrix[row][column+1], matrix[row][column-1]
    elif column == gridwidth-1:
        neighbors = matrix[row-1][column], matrix[row+1][column], matrix[row][column-1]
    else:
        neighbors = matrix[row-1][column], matrix[row][column+1], matrix[row+1][column], matrix[row][column-1]
    return neighbors






### Check map for islands / nonpathable areas. This reall doesn't work. 
def CheckMap(map):
    # Minimum number,criteria of adjacent territories. If 2, will run forever...
    crit = 1
    MapNorm = NormalizeMap(map)
    passed = True
    for row in xrange(gridheight):
        for column in xrange(gridwidth):
            neighbors = GetNeighbors(MapNorm,row,column)
            if sum(neighbors) < crit:
                print 'Islands!'
                passed = False
                break
        if passed == False:
            break
    if passed == True:
        print 'No islands!'
    return passed




passed = False
while passed == False:
    Map = CreateMap(NTeams, NTerritoriesPerTeam, NVoids, NStartingDicePerTeam)
    passed = CheckMap(Map)



print '(id,dice)'
for i in Map: print i




# Maybe for easier reading, assign indices 0,1 of Map with words instead
ID = 0
DICE = 1














### Return random roll of dice with number of sides. Has error handling.
def roll(dice,sides=4):
    if sides < 2 or dice < 1:
        raise ValueError('Roll at least one die with at least two sides.')
    return sum(random.randint(1, sides) for die in range(dice))


class Battle():
    def __init__(self, attacker, defender):
        # attacker = (team id, n dice)
        self.atk_id = attacker[0]
        self.atk_dice = attacker[1]
        self.def_id = defender[0]
        self.def_dice = defender[1]
    def winner(self):
        score = {}
        score[self.atk_id] = roll(self.atk_dice)
        score[self.def_id] = roll(self.def_dice)
        
        #if self.atk_id != self.def_id:
        print ' Attacker '+cnames[self.atk_id]+' rolled', str(self.atk_dice)+'d'+'4 =', score[self.atk_id]
        print ' Defender '+cnames[self.def_id]+' rolled', str(self.def_dice)+'d'+'4 =', score[self.def_id]

        '''
        if score[self.atk_id] > score[self.def_id]:
            print '  --> '+cnames[self.atk_id]+' won!'
        #else:
        #    print '  -> '+cnames[max(score, key=score.get)]+' won!'
        elif score[self.atk_id] <= score[self.def_id]:
            print '  --> '+cnames[self.atk_id]+' lost!'
        '''
        return score[self.atk_id], score[self.def_id]


# For testing. Attacker aa is Team 1, two dice...
'''
aa = (1,2)
dd = (2,2)
Battle(aa,dd).winner()
'''












#h = [(ndice, [(nsides, nsides+ndice) for nsides in xrange(1,10+1)]) for ndice in xrange(1,10+1)]





### Available colors of teams
#cnames = ['white','darkviolet', 'darkgreen', 'darkblue', 'yellow']
#cnames = ['white', 'palegreen', 'steelblue', 'firebrick', 'orchid']
cnames = ['white', 'navy', 'magenta', 'cyan','gold']
#cnames = ['white','blue', 'green', 'red', 'cyan', 'magenta', 'yellow']

cmargin = 'white'
cfont = 'black'

catkborder = 'green'
cdefborder = 'red'

cvoids = ['white']
#cterrs = ['blue','red','green','yellow']
cterrs = ['magenta','cyan', 'orange']
cnames = cvoids + cterrs


# Initialize font. You have to call this at the start if you want to use this module.
pg.font.init()
fontsize = squarewidth/3
font = pg.font.SysFont('monospace', fontsize)




# Screen width, height in pixels
screenwidth = gridwidth*squarewidth + nmargin*squaremargin
screenheight = gridheight*squareheight + nmargin*squaremargin
screen = pg.display.set_mode([screenwidth, screenheight])

# Set screen title
pg.display.set_caption(os.path.basename(__file__)[:-3])



# Set screen background, ie. margins
screen.fill(pg.Color(cmargin))

running = True
try:
    print ''
    #print 'Team \t Screen/Pixel \t Grid'

    while running:

        # Draw grid
        for row in xrange(gridheight):
            for column in xrange(gridwidth):
                color = pg.Color(cnames[Map[row][column][ID]])
                #if grid[row][column] == 0: pg.Color('white')
                #elif grid[row][column] == 0:
                #color = pg.Color('white')
                #if grid[row][column] == 1: color = pg.Color('palegreen')
                #if grid[row][column] == 1: color = pg.Color('orange')
                #if grid[row][column] == 2: color = pg.Color('red')
                #if grid[row][column] == 3: color = pg.Color('darkviolet')
                pg.draw.rect(screen, color, [(squarewidth + squaremargin)*column + squaremargin, 
                                             (squareheight + squaremargin)*row + squaremargin, 
                                              squarewidth, squareheight]) #,1)
                if Map[row][column][ID] != 0:
                    #if len(StartingDice[grid[row][column]-1]) > 0 :
                    ndice = Map[row][column][DICE] #.pop()
                    #ndice = random.randint(1,5) 
                    dice = font.render(str(ndice), False, pg.Color(cfont))
                    screen.blit(dice, ((squarewidth + squaremargin)*column + squaremargin + squarewidth/3,
                                        (squareheight + squaremargin)*row + squaremargin + squareheight/3))


        for event in pg.event.get():
            if event.type == pg.QUIT:
                running = False


            ### Click for attacking territory
            elif event.type == pg.MOUSEBUTTONDOWN:
                # pos from top left
                x1,y1 = pg.mouse.get_pos()
                column1 = x1/(squarewidth+squaremargin)
                row1 = y1/(squareheight+squaremargin)
                pg.draw.rect(screen, pg.Color(catkborder), [(squarewidth + squaremargin)*column1 + squaremargin,
                                             (squareheight + squaremargin)*row1 + squaremargin,
                                              squarewidth, squareheight],3)
                pg.display.update()

                #grid[row][column] = 1 - grid[row][column]
                #print 'Screen/Pixels: y = '+str(y),'x = '+str(x),'\t Grid: row = '+str(row),'col = '+str(column)
                c1 = cnames[Map[row1][column1][ID]]
                #print c1+'\t y = '+str(y1),'x = '+str(x1),'\t row = '+str(row1),'col = '+str(column1)


                ### Click for defending territory
                attacking = True
                while attacking:
                    for ev in pg.event.get(pg.MOUSEBUTTONDOWN):
                        x2,y2 = pg.mouse.get_pos()
                        column2 = x2/(squarewidth+squaremargin)
                        row2 = y2/(squareheight+squaremargin)
                        pg.draw.rect(screen, pg.Color(cdefborder), 
                                    [(squarewidth + squaremargin)*column2 + squaremargin, 
                                    (squareheight + squaremargin)*row2 + squaremargin,
                                    squarewidth, squareheight],3)
                        c2 = cnames[Map[row2][column2][ID]]
                        #print c2+'\t y = '+str(y2),'x = '+str(x2),'\t row = '+str(row2),'col = '+str(column2)
                        pg.display.update()

                        #if Map[row2][column2][ID] == Map[row1][column1][ID]: break

                        attacker = Map[row1][column1][ID], Map[row1][column1][DICE]
                        defender = Map[row2][column2][ID], Map[row2][column2][DICE]
                        atkscore, defscore = Battle(attacker,defender).winner()
                        attacking = False
   
                
                        ### Carnage post-battle
                        atkdicebeforebattle = Map[row1][column1][DICE]
                        defdicebeforebattle = Map[row2][column2][DICE]
 
                        # If attacker wins (def loses all, atk loses none)
                        if atkscore > defscore:
                            Map[row2][column2][ID] = Map[row1][column1][ID]
                            Map[row2][column2][DICE] = Map[row1][column1][DICE]-1
                            Map[row1][column1][DICE] = 1

                            #print '     '+c1+' ('+str(row1)+','+str(column1)+')', str(atkdicebeforebattle)+'->'+str(Map[row1][column1][DICE])
                            #print '     '+c2+' ('+str(row2)+','+str(column2)+')', str(defdicebeforebattle)+'->'+str(Map[row2][column2][DICE])


                        # If attacker loses (there're losses on both sides)
                        elif atkscore <= defscore:

                            # Atk's dice decrease by highest of diff/2 between atk/def ndice or dice score
                            Map[row1][column1][DICE] = (atkdicebeforebattle - \
                                    max(abs(atkscore-defscore),abs(atkdicebeforebattle-defdicebeforebattle)))/2
                            if Map[row1][column1][DICE] < 1: Map[row1][column1][DICE] = 1
                            elif Map[row1][column1][DICE] == atkdicebeforebattle: 
                                Map[row1][column1][DICE] -= 1
                            
                            # Def's dice decrease by lowest of diff/2 between atk/def ndice/2 or dice score/2
                            Map[row2][column2][DICE] = defdicebeforebattle - \
                                    min(abs(atkscore-defscore)/2,abs(atkdicebeforebattle-defdicebeforebattle)/2,
                                        abs(Map[row1][column1][DICE]-atkdicebeforebattle)/2)
                            if Map[row2][column2][DICE] < 1: Map[row2][column2][DICE] = 1
                            
 
                        pg.display.update()
            

            elif event.type == pg.KEYDOWN:
                if event.key == pg.K_RETURN:
                    #id = Map[row1][column1][ID]
                    for row in xrange(gridheight):
                        for column in xrange(gridwidth):
                            if Map[row][column][ID] == Map[row1][column1][ID]: #id
                                # Max number a square can have is number of territories. 13 for 4x4
                                if Map[row][column][DICE] < NTerritories:
                                    Map[row][column][DICE] += 1             
                    print ' End of '+cnames[Map[row1][column1][ID]]+'\'s turn'
                    print ''
                #pg.display.update() #Not sure this is necessary


        pg.display.update()#flip()

                    
        control = []
        for row in xrange(gridheight):
            for column in xrange(gridwidth):
                control.append(Map[row][column][ID])

        teamsremaining = set([control[i] for i in xrange(len(control)) if control[i] > 0])
        if len(teamsremaining) == 1:
            #print ''
            #print cnames[list(teamsremaining)[0]]+' wins the game!'
            #print ''
            running = False
            #pg.quit()
            #break

    if len(teamsremaining) == 1:
        print ''
        print cnames[list(teamsremaining)[0]]+' wins the game!'
        print ''


    pg.quit()

except SystemExit:
    pg.quit()




# For testing. Attacker aa is Team 1, two dice...
'''
aa = (1,2)
dd = (2,2)
Battle(aa,dd).winner()
'''


control = []
for row in xrange(gridheight):
    for column in xrange(gridwidth):
        control.append(Map[row][column][ID])
        #if len(set(control)) > 1: break        
    #if len(set(control)) > 1: break

teamsremaining = set([control[i] for i in xrange(len(control)) if control[i] > 0])
if len(teamsremaining) == 1:
    print cnames[list(teamsremaining)[0]]+' wins the game!' 

