import pygame as pg
import random
import os


import math

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
    quit()
# Redefine math.f() to f()
def sqrt(x):
    return math.sqrt(x)
def sin(x):
    return math.sin(x)
def cos(x):
    return math.cos(x)
# Redefine pi to something shorter
pi = math.pi
print ''





#########################################################################################################
# Making simple Dice Battles game. No hex, no voronoi. Just squares... for now.
# See DiceBattles7 and 6 (and lower) for advances in those aspects.
#########################################################################################################






# n squares wide, high; and width, height of square in pixels
gridwidth = 6
gridheight = 5
squarewidth = squareheight = 70

# border size between squares in pixels
nmargin = gridwidth+1
squaremargin = 1






# Side length and flat-to-flat width
hexgridwidth = 20
hexgridheight = 5

hexmargin = 12
#nhexmargin = hexgridwidth+1

hexsidelength = 15
hexwidth = int(round(sqrt(3)*hexsidelength))
hexheight = int(round(2/sqrt(3)*hexwidth))




# Flat topped. From top-left corner 0
#   0__1
# 5/    \2
#  \4__3/


# Pointy topped. From center.
# See http://pritschet.me/media/uploads/python/cairo-img-00.png
def Hex(l,center):
    # sidelength l = int, pixels
    # center = (x,y), pixels
    cx,cy = center
    D = l*2.
    a = D/4.
    b = sqrt(3)*a
    # .--> x
    # |
    # v    0
    # y  5/ \1
    #    | . |
    #    4\ /2
    #      3
    x0,y0 = cx,  cy-2*a
    x1,y1 = cx+b,cy-a
    x2,y2 = cx+b,cy+a
    x3,y3 = cx,  cy+2*a
    x4,y4 = cx-b,cy+a
    x5,y5 = cx-b,cy-a
    return [ [int(round(eval('x'+str(i)))), int(round(eval('y'+str(i))))] for i in xrange(6) ]
    



#hexgridheight2 = int(1.25*hexgridheight) # int(round(4/3.*hexgridheight))
#hexgridheight2 = int(sqrt(3)*hexgridheight)

#hexgridheight2 = hexgridheight

hexgrid = [[0]*hexgridwidth for i in xrange(hexgridheight)]
for y in xrange(hexgridheight):
    for x in xrange(hexgridwidth):
        hexgrid[y][x] = Hex(hexsidelength, (hexwidth/2.+hexmargin + x*(hexwidth+2*hexmargin) + 
                                         y%2*(hexwidth/2+hexmargin), 
                                         hexheight/2.+hexmargin + y*(hexheight*3/4.+ 2*hexmargin)))        


#hexscreenwidth = hexgridwidth*hexwidth + 2*nhexmargin*hexmargin + hexwidth/2
#hexscreenheight = int(hexgridheight/2.*1.75* hexheight) + 2*nhexmargin*hexmargin 
#int(hexgridheight2/2.*1.75 * hexheight) + nhexmargin*hexmargin #+ hexheight/4

hexscreenwidth = hexgridwidth*hexwidth + 2*(hexgridwidth+1)*hexmargin + hexwidth/2
#hexscreenheight = int(hexgridheight * hexheight/2.*1.75) + (hexgridheight+1)*hexmargin
#hexscreenheight = int(hexgridheight*hexheight*3/4.) + 2*(hexgridheight+1)*hexmargin  #/2.*1.75)
hexscreenheight = int(hexgridheight/2.*1.75 * hexheight) + 2*(hexgridheight+1)*hexmargin
hexscreen = pg.display.set_mode([hexscreenwidth, hexscreenheight])
hexscreen.fill(pg.Color('red'))

for y in xrange(hexgridheight):
    for x in xrange(hexgridwidth):
        pg.draw.polygon(hexscreen, pg.Color('white'), hexgrid[y][x] )

pg.display.update()
















# N number of teams/colors/identifiers.
NTeams = 3


### N numbr of total Regions/Territories/Voids on the map. 
# Region == Territory (capturable) or Void (empty/obstruction)
NRegions = gridwidth*gridheight
# Percent/100 of map that territories cover2
if NRegions < 25: coverage = 0.8
else: coverage = 0.6
NTerritories = int(round(NRegions*coverage))

### N number of Territories each team/color/identifier begins with.
NTerritoriesPerTeam = NTerritories/NTeams

NVoids = NRegions- NTerritoriesPerTeam*NTeams


### Generate dice for each Team/Territory
NStartingDicePerTeam = 3*NTerritoriesPerTeam










# From https://stackoverflow.com/questions/3589214/generate-multiple-random-numbers-to-equal-a-value-in-python
# Return a randomly chosen list of n positive integers summing to total.
# Each such list is equally likely to occur.
def CSS(n, total):
    dividers = sorted(random.sample(xrange(1, total), n - 1))
    return [a - b for a, b in zip(dividers + [total], [0] + dividers)]





def CreateMap(nteams, nterritoriesperteam, nvoids, nstartingdiceperteam):
    # Create starting locations list of form [1,1,1,1, 2,2,2,2,..., 0,0,...]
    # Team = 1, 2... Void = 0
    StartingLocations = sorted(range(1,nteams+1)*nterritoriesperteam) + [0]*nvoids
    # Create dice at starting locations of form [2,2,5,3, ..., 0,0,...]. 2+2+5+3 = 12 = NStartginDice here
    StartingLocationsDice = [CSS(nterritoriesperteam,nstartingdiceperteam) for i in xrange(nteams)]
    StartingLocationsDice = [item for sublist in StartingLocationsDice for item in sublist] + [0]*nvoids

    # Zip together, shuffle, recast tuple(a,b) to list[a,b], and define as Map in shape of grid
    zipped = zip(StartingLocations,StartingLocationsDice)
    zipped = [list(i) for i in zipped]
    random.shuffle(zipped)
    
    map = [zipped[gridwidth*i:gridwidth*(i+1)] for i in xrange(gridheight)]
    return map






# Normalize Map so all non0 are 1, and all 0 are remain 0
def NormalizeMap(map):
    ID = 0
    mapnorm = [[int(round(j[ID]/(j[ID]+0.1))) for j in i] for i in map]
    return mapnorm






# Psuedohardcoding how to get neighbors. For reading reference: getting clockwise from top.
def GetNeighbors(matrix,row,column):
    if row == 0:
        if column == 0: neighbors = matrix[row][column+1], matrix[row+1][column]
        elif column == gridwidth-1: neighbors = matrix[row+1][column], matrix[row][column-1]
        else: neighbors = matrix[row][column+1], matrix[row+1][column], matrix[row][column-1]
    elif column == 0:
        if row == gridheight-1: neighbors = matrix[row-1][column], matrix[row][column+1]
        else: neighbors = matrix[row-1][column], matrix[row][column+1], matrix[row+1][column]
    elif row == gridheight-1:
        if column == gridwidth-1: neighbors = matrix[row-1][column], matrix[row][column-1]
        else: neighbors = matrix[row-1][column], matrix[row][column+1], matrix[row][column-1]
    elif column == gridwidth-1:
        neighbors = matrix[row-1][column], matrix[row+1][column], matrix[row][column-1]
    else:
        neighbors = matrix[row-1][column], matrix[row][column+1], matrix[row+1][column], matrix[row][column-1]
    return neighbors






### Check map for islands / nonpathable areas. This really doesn't work; islands >1 still form.
def CheckMap(map):
    # Minimum number,criteria of adjacent territories. If 2, will run forever...
    crit = 1
    MapNorm = NormalizeMap(map)
    passed = True
    for row in xrange(gridheight):
        for column in xrange(gridwidth):
            neighbors = GetNeighbors(MapNorm,row,column)
            if sum(neighbors) < crit:
                print 'Islands!'
                passed = False
                break
        if passed == False:
            break
    if passed == True:
        print 'No islands!'
    return passed




passed = False
while passed == False:
    Map = CreateMap(NTeams, NTerritoriesPerTeam, NVoids, NStartingDicePerTeam)
    passed = CheckMap(Map)



print '[id,dice]'
for i in Map: print i




# Maybe for easier reading, assign indices 0,1 of Map with words instead
ID = 0
DICE = 1














### Return random roll of dice with number of sides. Has error handling.
def roll(dice,sides=4):
    if sides < 2 or dice < 1:
        raise ValueError('Roll at least one die with at least two sides.')
    return sum(random.randint(1, sides) for die in range(dice))


class Battle():
    def __init__(self, attacker, defender):
        # attacker = (team id, n dice)
        self.atk_id = attacker[ID]
        self.atk_dice = attacker[DICE]
        self.def_id = defender[ID]
        self.def_dice = defender[DICE]
    def winner(self):
        score = {}
        score[self.atk_id] = roll(self.atk_dice)
        score[self.def_id] = roll(self.def_dice)
        
        if self.def_id != self.atk_id:
            print ' Attacker '+cnames[self.atk_id]+' rolled', str(self.atk_dice)+'d'+'4 =', score[self.atk_id]
            print ' Defender '+cnames[self.def_id]+' rolled', str(self.def_dice)+'d'+'4 =', score[self.def_id]

        '''
        if score[self.atk_id] > score[self.def_id]:
            print '  --> '+cnames[self.atk_id]+' won!'
        #else:
        #    print '  -> '+cnames[max(score, key=score.get)]+' won!'
        elif score[self.atk_id] <= score[self.def_id]:
            print '  --> '+cnames[self.atk_id]+' lost!'
        '''
        return score[self.atk_id], score[self.def_id]






### Post-battle aftermath
def Aftermath(attacker, atkscore, defender, defscore):
    # attacker = Map[row1][column1][ID], Map[row1][column1][DICE]
    # atkscore = int
    atkID = attacker[ID]
    atkDICE_i = attacker[DICE]
    defID = defender[ID]
    defDICE_i = defender[DICE]

    if defID == atkID:
        return atkID,atkDICE_i, defID,defDICE_i

    else:
        # If attacker wins (def loses all, atk loses none; maybe the idea being that atk recruits def survivors)
        if atkscore > defscore:
            defID = atkID
            defDICE = atkDICE_i-1
            atkDICE = 1

        # If attacker loses (there are losses on both sides)
        elif atkscore <= defscore:

            # Atk's dice decrease by highest of diff/2 between atk/def ndice or dice score
            atkDICE = (atkDICE_i - max(abs(atkscore-defscore), abs(atkDICE_i-defDICE_i)))/2
            if atkDICE < 1: atkDICE = 1
            elif atkDICE == atkDICE_i:
                atkDICE -= 1

            # Def's dice decrease by lowest of diff between atk/def ndice/2 or dice score/2
            defDICE = defDICE_i - \
                    min(abs(atkscore-defscore)/2, abs(atkDICE_i-defDICE_i)/2, abs(atkDICE-atkDICE_i)/2)
            if defDICE < 1: defDICE = 1

        return atkID,atkDICE, defID,defDICE






#h = [(ndice, [(nsides, nsides+ndice) for nsides in xrange(1,10+1)]) for ndice in xrange(1,10+1)]





### Available colors of teams
#cnames = ['white','darkviolet', 'darkgreen', 'darkblue', 'yellow']
#cnames = ['white', 'palegreen', 'steelblue', 'firebrick', 'orchid']
cnames = ['white', 'navy', 'magenta', 'cyan','gold']
#cnames = ['white','blue', 'green', 'red', 'cyan', 'magenta', 'yellow']

cmargin = 'white'
cfont = 'black'

catkborder = 'green'
cdefborder = 'red'
bordersize = 3

cvoids = ['white']
#cterrs = ['blue','red','green','yellow']
cterrs = ['magenta','cyan', 'orange']
cnames = cvoids + cterrs


# Initialize font. You have to call this at the start if you want to use this module.
pg.font.init()
fontsize = squarewidth/3
font = pg.font.SysFont('monospace', fontsize)


# Screen width, height in pixels
screenwidth = gridwidth*squarewidth + nmargin*squaremargin
screenheight = gridheight*squareheight + nmargin*squaremargin
screen = pg.display.set_mode([screenwidth, screenheight])

# Set screen title
pg.display.set_caption(os.path.basename(__file__)[:-3])



# Set screen background, ie. margins
screen.fill(pg.Color(cmargin))

running = True
try:
    while running:

        # Draw grid
        for row in xrange(gridheight):
            for column in xrange(gridwidth):
                color = pg.Color(cnames[Map[row][column][ID]])
                pg.draw.rect(screen, color, [(squarewidth + squaremargin)*column + squaremargin, 
                                             (squareheight + squaremargin)*row + squaremargin, 
                                              squarewidth, squareheight])
                if Map[row][column][ID] != 0:
                    #if len(StartingDice[grid[row][column]-1]) > 0 :
                    ndice = Map[row][column][DICE]
                    dice = font.render(str(ndice), False, pg.Color(cfont))
                    screen.blit(dice, ((squarewidth + squaremargin)*column + squaremargin + squarewidth/3,
                                        (squareheight + squaremargin)*row + squaremargin + squareheight/3))


        for event in pg.event.get():
            if event.type == pg.QUIT:
                running = False


            ### Click for attacking territory
            elif event.type == pg.MOUSEBUTTONDOWN:
                x1,y1 = pg.mouse.get_pos()
                column1 = x1/(squarewidth+squaremargin)
                row1 = y1/(squareheight+squaremargin)
                pg.draw.rect(screen, pg.Color(catkborder), [(squarewidth + squaremargin)*column1 + squaremargin,
                                             (squareheight + squaremargin)*row1 + squaremargin,
                                              squarewidth, squareheight],bordersize)
                pg.display.update()

                #c1 = cnames[Map[row1][column1][ID]]
                #print c1+'\t y = '+str(y1),'x = '+str(x1),'\t row = '+str(row1),'col = '+str(column1)


                ### Click for defending territory
                attacking = True
                while attacking:
                    for ev in pg.event.get(pg.MOUSEBUTTONDOWN):
                        x2,y2 = pg.mouse.get_pos()
                        column2 = x2/(squarewidth+squaremargin)
                        row2 = y2/(squareheight+squaremargin)

                        if abs(row1-row2) + abs(column1-column2) > 1:
                            attacking = False
                            break

                        pg.draw.rect(screen, pg.Color(cdefborder), 
                                    [(squarewidth + squaremargin)*column2 + squaremargin, 
                                    (squareheight + squaremargin)*row2 + squaremargin,
                                    squarewidth, squareheight],bordersize)
                        #c2 = cnames[Map[row2][column2][ID]]
                        #print c2+'\t y = '+str(y2),'x = '+str(x2),'\t row = '+str(row2),'col = '+str(column2)
                        pg.display.update()

                        #if Map[row2][column2][ID] == Map[row1][column1][ID]: break
                        attacker = Map[row1][column1][ID], Map[row1][column1][DICE]
                        defender = Map[row2][column2][ID], Map[row2][column2][DICE]
                        atkscore, defscore = Battle(attacker,defender).winner()
                        attacking = False
   
                

                        ### Post-battle aftermath
                        Map[row1][column1][ID], Map[row1][column1][DICE], \
                        Map[row2][column2][ID], Map[row2][column2][DICE] = \
                            Aftermath(attacker, atkscore, defender, defscore)
                        '''
                        ### Carnage post-battle
                        atkdicebeforebattle = Map[row1][column1][DICE]
                        defdicebeforebattle = Map[row2][column2][DICE]
 
                        # If attacker wins (def loses all, atk loses none)
                        if atkscore > defscore:
                            Map[row2][column2][ID] = Map[row1][column1][ID]
                            Map[row2][column2][DICE] = Map[row1][column1][DICE]-1
                            Map[row1][column1][DICE] = 1

                        # If attacker loses (there're losses on both sides)
                        elif atkscore <= defscore:
                            # Atk's dice decrease by highest of diff/2 between atk/def ndice or dice score
                            Map[row1][column1][DICE] = (atkdicebeforebattle - \
                                    max(abs(atkscore-defscore),abs(atkdicebeforebattle-defdicebeforebattle)))/2
                            if Map[row1][column1][DICE] < 1: Map[row1][column1][DICE] = 1
                            elif Map[row1][column1][DICE] == atkdicebeforebattle: 
                                Map[row1][column1][DICE] -= 1
                            # Def's dice decrease by lowest of diff/2 between atk/def ndice/2 or dice score/2
                            Map[row2][column2][DICE] = defdicebeforebattle - \
                                    min(abs(atkscore-defscore)/2,abs(atkdicebeforebattle-defdicebeforebattle)/2,
                                        abs(Map[row1][column1][DICE]-atkdicebeforebattle)/2)
                            if Map[row2][column2][DICE] < 1: Map[row2][column2][DICE] = 1
                        '''    
                        # And redraw margins (background) to remove atk/def borders...
                        #screen.fill(pg.Color(cmargin)) 
                        pg.display.update()
            

            elif event.type == pg.KEYDOWN:
                if event.key == pg.K_RETURN:
                    #id = Map[row1][column1][ID]
                    for row in xrange(gridheight):
                        for column in xrange(gridwidth):
                            if Map[row][column][ID] == Map[row1][column1][ID]: #id
                                # Max number a square can have is number of territories. 13 for 4x4
                                if Map[row][column][DICE] < NTerritories:
                                    Map[row][column][DICE] += 1             
                    print ' End of '+cnames[Map[row1][column1][ID]]+'\'s turn'
                    print ''

        pg.display.update()#flip()

                    
        control = []
        for row in xrange(gridheight):
            for column in xrange(gridwidth):
                control.append(Map[row][column][ID])

        teamsremaining = set([control[i] for i in xrange(len(control)) if control[i] > 0])
        if len(teamsremaining) == 1:
            print ''
            print cnames[list(teamsremaining)[ID]]+' wins the game!'
            print ''
            running = False
            #pg.quit()
            #break

    pg.quit()

except SystemExit:
    pg.quit()



