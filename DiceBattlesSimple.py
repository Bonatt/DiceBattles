import pygame as pg
import random
import os


import math

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
    quit()
# Redefine math.f() to f()
def sqrt(x):
    return math.sqrt(x)
def sin(x):
    return math.sin(x)
def cos(x):
    return math.cos(x)
# Redefine pi to something shorter
pi = math.pi
print('')





#########################################################################################################
# Making simple Dice Battles game. No hex, no voronoi. Just squares... for now.
# See DiceBattles7 and 6 (and lower) for advances in those aspects.
#########################################################################################################






# Playfield is w,h squares wide, high; and width, height of square in pixels
gridwidth = 6
gridheight = 5
squarewidth = squareheight = 70

# border size between squares in pixels
nmargin = gridwidth+1
squaremargin = 1











# N number of teams/colors/identifiers.
# May only be as large as "cterrs" below. Add more if necessary.
NTeams = 2


### N numbr of total Regions/Territories/Voids on the map. 
# Region == Territory (capturable) or Void (empty/obstruction)
NRegions = gridwidth*gridheight
# Percent/100 of map that territories cover
if NRegions < 25: coverage = 0.8
else: coverage = 0.6
NTerritories = int(round(NRegions*coverage))

### N number of Territories each team/color/identifier begins with.
NTerritoriesPerTeam = NTerritories//NTeams

NVoids = NRegions- NTerritoriesPerTeam*NTeams


### Generate dice for each Team/Territory
NStartingDicePerTeam = 3*NTerritoriesPerTeam










# From https://stackoverflow.com/questions/3589214/generate-multiple-random-numbers-to-equal-a-value-in-python
# Return a randomly chosen list of n positive integers summing to total.
# Each such list is equally likely to occur.
def CSS(n, total):
    dividers = sorted(random.sample(range(1, int(total)), int(n) - 1))
    return [a - b for a, b in zip(dividers + [int(total)], [0] + dividers)]





def CreateMap(nteams, nterritoriesperteam, nvoids, nstartingdiceperteam):
    # Create starting locations list of form [1,1,1,1, 2,2,2,2,..., 0,0,...]
    # Team = 1, 2... Void = 0
    StartingLocations = sorted(list(range(1,int(nteams)+1))*int(nterritoriesperteam)) + [0]*int(nvoids)
    # Create dice at starting locations of form [2,2,5,3, ..., 0,0,...]. 2+2+5+3 = 12 = NStartginDice here
    StartingLocationsDice = [CSS(nterritoriesperteam,nstartingdiceperteam) for i in range(nteams)]
    StartingLocationsDice = [item for sublist in StartingLocationsDice for item in sublist] + [0]*int(nvoids)

    # Zip together, shuffle, recast tuple(a,b) to list[a,b], and define as Map in shape of grid
    zipped = zip(StartingLocations,StartingLocationsDice)
    zipped = [list(i) for i in zipped]
    random.shuffle(zipped)
    
    map = [zipped[gridwidth*i:gridwidth*(i+1)] for i in range(gridheight)]
    return map






# Normalize Map so all non0 are 1, and all 0 are remain 0
def NormalizeMap(map):
    ID = 0
    mapnorm = [[int(round(j[ID]/(j[ID]+0.1))) for j in i] for i in map]
    return mapnorm






# Psuedohardcoding how to get neighbors. For reading reference: getting clockwise from top.
def GetNeighbors(matrix,row,column):
    if row == 0:
        if column == 0: neighbors = matrix[row][column+1], matrix[row+1][column]
        elif column == gridwidth-1: neighbors = matrix[row+1][column], matrix[row][column-1]
        else: neighbors = matrix[row][column+1], matrix[row+1][column], matrix[row][column-1]
    elif column == 0:
        if row == gridheight-1: neighbors = matrix[row-1][column], matrix[row][column+1]
        else: neighbors = matrix[row-1][column], matrix[row][column+1], matrix[row+1][column]
    elif row == gridheight-1:
        if column == gridwidth-1: neighbors = matrix[row-1][column], matrix[row][column-1]
        else: neighbors = matrix[row-1][column], matrix[row][column+1], matrix[row][column-1]
    elif column == gridwidth-1:
        neighbors = matrix[row-1][column], matrix[row+1][column], matrix[row][column-1]
    else:
        neighbors = matrix[row-1][column], matrix[row][column+1], matrix[row+1][column], matrix[row][column-1]
    return neighbors






### Check map for islands / nonpathable areas. This really doesn't work. 
def CheckMap(map):
    # Minimum number,criteria of adjacent territories. If 2, will run forever...
    crit = 1
    MapNorm = NormalizeMap(map)
    passed = True
    for row in range(gridheight):
        for column in range(gridwidth):
            neighbors = GetNeighbors(MapNorm,row,column)
            if sum(neighbors) < crit:
                print('Islands!')
                passed = False
                break
        if passed == False:
            break
    if passed == True:
        print('No islands!')
    return passed




passed = False
while passed == False:
    Map = CreateMap(NTeams, NTerritoriesPerTeam, NVoids, NStartingDicePerTeam)
    passed = CheckMap(Map)



print('(id,dice)')
for i in Map: print(i)




# Maybe for easier reading, assign indices 0,1 of Map with words instead
ID = 0
DICE = 1














### Return random roll of dice with number of sides. Has error handling.
def roll(dice,sides=4):
    if sides < 2 or dice < 1:
        raise ValueError('Roll at least one die with at least two sides.')
    return sum(random.randint(1, sides) for die in range(dice))


class Battle():
    def __init__(self, attacker, defender):
        # attacker = (team id, n dice)
        self.atk_id = attacker[ID]
        self.atk_dice = attacker[DICE]
        self.def_id = defender[ID]
        self.def_dice = defender[DICE]
    def winner(self):
        score = {}
        score[self.atk_id] = roll(self.atk_dice)
        score[self.def_id] = roll(self.def_dice)
        
        #if self.atk_id != self.def_id:
        print(' Attacker '+cnames[self.atk_id]+' rolled...\t', str(self.atk_dice)+'d'+'4 =', score[self.atk_id])
        print('   Defender '+cnames[self.def_id]+' rolled...\t', str(self.def_dice)+'d'+'4 =', score[self.def_id])

        '''
        if score[self.atk_id] > score[self.def_id]:
            print('  --> '+cnames[self.atk_id]+' won!')
        #else:
        #    print '  -> '+cnames[max(score, key=score.get)]+' won!'
        elif score[self.atk_id] <= score[self.def_id]:
            print('  --> '+cnames[self.atk_id]+' lost!')
        '''
        return score[self.atk_id], score[self.def_id]














#h = [(ndice, [(nsides, nsides+ndice) for nsides in range(1,10+1)]) for ndice in range(1,10+1)]




# Border and font colors
cmargin = 'white'
cfont = 'black'

# atk and def highlight border colors
catkborder = 'green'
cdefborder = 'red'

# Region colods
cvoids = ['white']
### Available colors of teams
#cterrs = ['darkviolet', 'darkgreen', 'darkblue', 'yellow']
#cterrs = ['palegreen', 'steelblue', 'firebrick', 'orchid']
#cterrs = ['navy', 'magenta', 'cyan','gold']
#cterrs = ['blue', 'green', 'red', 'cyan', 'magenta', 'yellow']
#cterrs = ['blue','red','green','yellow']
cterrs = ['magenta','cyan', 'orange', 'gold', 'green','purple']
cnames = cvoids + cterrs


# Initialize font. You have to call this at the start if you want to use this module.
pg.font.init()
fontsize = squarewidth/3
font = pg.font.SysFont('monospace', int(fontsize))


# Screen width, height in pixels
screenwidth = gridwidth*squarewidth + nmargin*squaremargin
screenheight = gridheight*squareheight + nmargin*squaremargin
screen = pg.display.set_mode([screenwidth, screenheight])

# Set screen title
pg.display.set_caption(os.path.basename(__file__)[:-3])



# Set screen background, ie. margins
screen.fill(pg.Color(cmargin))

running = True
try:
    while running:

        # Draw grid
        for row in range(gridheight):
            for column in range(gridwidth):
                color = pg.Color(cnames[Map[row][column][ID]])
                pg.draw.rect(screen, color, [(squarewidth + squaremargin)*column + squaremargin, 
                                             (squareheight + squaremargin)*row + squaremargin, 
                                              squarewidth, squareheight])
                if Map[row][column][ID] != 0:
                    #if len(StartingDice[grid[row][column]-1]) > 0 :
                    ndice = Map[row][column][DICE]
                    dice = font.render(str(ndice), False, pg.Color(cfont))
                    screen.blit(dice, ((squarewidth + squaremargin)*column + squaremargin + squarewidth/3,
                                        (squareheight + squaremargin)*row + squaremargin + squareheight/3))


        for event in pg.event.get():
            if event.type == pg.QUIT:
                running = False


            ### Click for attacking territory
            elif event.type == pg.MOUSEBUTTONDOWN:
                x1,y1 = pg.mouse.get_pos()
                column1 = x1//(squarewidth+squaremargin)
                row1 = y1//(squareheight+squaremargin)
    
                ### Check if attacking territory is actually a void
                if Map[row1][column1][ID] == 0:
                    continue

                pg.draw.rect(screen, pg.Color(catkborder), [(squarewidth + squaremargin)*column1 + squaremargin,
                                             (squareheight + squaremargin)*row1 + squaremargin,
                                              squarewidth, squareheight],3)
                pg.display.update()

                c1 = cnames[Map[row1][column1][ID]]
                #print c1+'\t y = '+str(y1),'x = '+str(x1),'\t row = '+str(row1),'col = '+str(column1)


                ### Click for defending territory
                attacking = True
                while attacking:
                    for ev in pg.event.get(pg.MOUSEBUTTONDOWN):
                        x2,y2 = pg.mouse.get_pos()
                        column2 = x2//(squarewidth+squaremargin)
                        row2 = y2//(squareheight+squaremargin)

                        # Check if same team or not adjacent (euclidian dist >= sqrt(2)) or is void
                        if Map[row2][column2][ID] == Map[row1][column1][ID] or \
                           sqrt(abs(column2-column1) + abs(row2-row1)) >= 2 or \
                           Map[row2][column2][ID] == 0:
                            attacking = False
                            continue                    

                        # Draw active border
                        pg.draw.rect(screen, pg.Color(cdefborder), 
                                    [(squarewidth + squaremargin)*column2 + squaremargin, 
                                    (squareheight + squaremargin)*row2 + squaremargin,
                                    squarewidth, squareheight],3)
                        c2 = cnames[Map[row2][column2][ID]]
                        #print c2+'\t y = '+str(y2),'x = '+str(x2),'\t row = '+str(row2),'col = '+str(column2)
                        pg.display.update()


                        ### Pre-battle
                        atkdicebeforebattle = Map[row1][column1][DICE]
                        defdicebeforebattle = Map[row2][column2][DICE]

                        ### Battle
                        #if Map[row2][column2][ID] == Map[row1][column1][ID]: break
                        attacker = Map[row1][column1][ID], atkdicebeforebattle
                        defender = Map[row2][column2][ID], defdicebeforebattle
                        atkscore, defscore = Battle(attacker,defender).winner()
                        attacking = False
   
                        ### Carnage post-battle
                        # If attacker wins (def loses all, atk loses none)
                        if atkscore > defscore:
                            Map[row2][column2][ID] = Map[row1][column1][ID]
                            Map[row2][column2][DICE] = Map[row1][column1][DICE]-1
                            Map[row1][column1][DICE] = 1


                        # If attacker loses (there're losses on both sides)
                        elif atkscore <= defscore:

                            # Atk's dice decrease by highest of diff/2 between atk/def ndice or dice score
                            Map[row1][column1][DICE] = (atkdicebeforebattle - \
                                    max(abs(atkscore-defscore),abs(atkdicebeforebattle-defdicebeforebattle)))//2
                            if Map[row1][column1][DICE] < 1: Map[row1][column1][DICE] = 1
                            elif Map[row1][column1][DICE] == atkdicebeforebattle: 
                                Map[row1][column1][DICE] -= 1
                            if Map[row1][column1][DICE] < 1: Map[row1][column1][DICE] = 1
                            
                            # Def's dice decrease by lowest of diff/2 between atk/def ndice/2 or dice score/2
                            Map[row2][column2][DICE] = defdicebeforebattle - \
                                    min(abs(atkscore-defscore)//2,abs(atkdicebeforebattle-defdicebeforebattle)//2,
                                        abs(Map[row1][column1][DICE]-atkdicebeforebattle)//2)
                            if Map[row2][column2][DICE] < 1: Map[row2][column2][DICE] = 1
                            
 
                        pg.display.update()
            
            ### ENTER to end turn and each territory of team just played increases by 1.
            # Only seems to work when game window is in focus (i.e. don't click terminal window to hit enter; just hit enter)
            elif event.type == pg.KEYDOWN:
                if event.key == pg.K_RETURN:
                    #id = Map[row1][column1][ID]
                    for row in range(gridheight):
                        for column in range(gridwidth):
                            if Map[row][column][ID] == Map[row1][column1][ID]: #id
                                # Max number a square can have is number of territories. 13 for 4x4
                                if Map[row][column][DICE] < NTerritories:
                                    Map[row][column][DICE] += 1             
                    print(' End of '+cnames[Map[row1][column1][ID]]+'\'s turn')
                    print('')


        pg.display.update()#flip()

                    
        control = []
        for row in range(gridheight):
            for column in range(gridwidth):
                control.append(Map[row][column][ID])

        teamsremaining = set([control[i] for i in range(len(control)) if control[i] > 0])
        if len(teamsremaining) == 1:
            print('')
            print(cnames[list(teamsremaining)[0]]+' wins the game!')
            print('')
            running = False
            #pg.quit()
            #break

    pg.quit()

except SystemExit:
    pg.quit()



