### Reimplementing "Dice Battles" with PyGame

Within the Windows Phone App Store there is a game called [Dice Battles](https://www.microsoft.com/en-us/store/p/dice-battles/9nblggh081pm).
It is like [Risk](https://en.wikipedia.org/wiki/Risk_(game)) but less involved and more random. Here is a screenshot:

![Screenshot, them](/Reference/WindowsPhoneDiceBattlesScreenshot.png)

I decided to reimplement this in Python, and to do that I employed [PyGame](https://www.pygame.org/).

I first attempted to generate a suitable world via Voronoi and hexagons. That did not work out so I took a different approach: 
just a simple square grid. I think this succeeded fairly well (playable, at least), 
but there is no pre-game GUI for certain parameter selection.
I then again returned to employing hexagons but only did some experimentation before shelving the full project altogether.


### How To Run

One will need to install PyGame via, for example:
```
pip install pygame
```

When/if installed, the game can be run like any other Python file:
```
python DiceBattlesSimple.py
```
![Two players](/Reference/DiceBattlesSimpleScreenshot2.png)
![Two players w/ island](/Reference/DiceBattlesSimpleScreenshot2i.png)
![Three players](/Reference/DiceBattlesSimpleScreenshot3.png)
![Five players](/Reference/DiceBattlesSimpleScreenshot5.png)

The board/playfield/map is created randomly (for 2, 3, 5 players respectively, above). 
A weak check for noncontinental islands does exist
but is not powerful enough to eliminate there occurance from all maps. Alas.
You'll just have to keep rerunning the program if necessary.
This was generated with the default parameters:
```python
# Playfield is w,h squares wide,high
gridwidth = 6
gridheight = 5

# N number of teams/colors/identifiers
NTeams = 2

# N numbr of total Regions/Territories/Voids on the map
# Region == Territory (capturable) or Void (empty/obstruction)
NRegions = gridwidth*gridheight
# Percent/100 of map that territories cover
if NRegions < 25: coverage = 0.8
else: coverage = 0.6
NTerritories = int(round(NRegions*coverage))

# N number of Territories each team/color/identifier begins with.
NTerritoriesPerTeam = NTerritories//NTeams

NVoids = NRegions- NTerritoriesPerTeam*NTeams

# Generate dice for each Team/Territory
NStartingDicePerTeam = 3*NTerritoriesPerTeam
```
All teams will begin the game with the same number of controlled territories and the same number of total dice, 
but the number of dice on each territory is randomly determined.

### How to Play

In its currently-released form, there is no AI and any number of human plays can play (up to the number of unique colors, I suppose).
And play is pretty free-form: no one is software-ically locked into a color so everyone will have to play honestly and carefully. 
Alas.

White "squares" are impassible terrain.
Colored squares represent territories under the control of that color. Each color represents a team. 
The number on each square represents the number of d-sided dice (d4 by default here, d6 by default in the original) in the territory. 
The number of dice represents that territorys attack or defense power, i.e. number of diced rolled:
the more dice, the greater the probability that square will successfully attack or defend against an adjacent enemy territory.

The goal of the game is to control the entire map by attacking other territories not under your control. 
This is done one at a time by clicking on one of your territories and then clicking on one adjacent 
enemy territory you wish to attack. 

If the dice roll of the attacking territory is _higher_ than roll of the defending territory, you win that battle.
All dice on the defending territory are removed and you claim that territory. 
All dice from the attacking territory (leaving one behind) move to that territory.

If the dice roll of the attacking territory is _lower_ or _tied_ with the roll of the defending territory, you lose the battle.
All but one die on the attacking territory are removed.

Caveat: the above win/loss rules are actually subtely different but that -- from observation -- is how the original game plays.

Only territories with greater than one die may attack. Any territory may defend. 
You may attack any number of times and with any number of territories you want during your turn. 
When you want your turn to end, press Enter and every territory under your control will gain one die.
It will then be the next players turn.

The game may or may not stop itself at time of win condition.

